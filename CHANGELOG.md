# Changelog

## [1.0.3] - 17.10.2019
- Adjust Homepage Boxes
- CSS Cleanup
- Restructuring of App Layout Components

## [1.0.2] - 17.10.2019
- Added topbar code
- Finished laws reimplementation

## [1.0.1] - 08.10.2019
- Changes here (Added/moved)

### FIX
- Fixes here

## [1.0.0] - 08.10.2019
- Changelog file created.