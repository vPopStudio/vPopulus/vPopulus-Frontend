import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { Style, Fill, Stroke } from 'ol/style';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;

  map: any = {};
  regions: any[] = [];
  isLoading = false;

  entityRegion = { capitalCoords: [0, 0], id: 0 };

  selectedRegion = undefined;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadRegions();
  }

  loadRegions() {
    this.isLoading = true;
    this.httpService.get2(`/api/maps`, this.token).subscribe(r => {
      this.regions = r.regions.filter(x => x.mapPath !== null);
      if(r.entityRegion != undefined)
        this.entityRegion = r.entityRegion;
      this.regions.forEach(region => {
        var paths = [];
        region.mapPath.points.forEach(coords => {
          paths.push([coords.lng, coords.lat]);
        });
        region.unifiedPaths = paths;
      });
      this.isLoading = false;
    }, error => {
      console.log('err', error)
    });
  }

  mouseOver(event) {
    var pixel = event.map.getEventPixel(event.originalEvent);
    if (event.map.hasFeatureAtPixel(pixel) === false) {
      this.selectedRegion = undefined;
    } else {
      event.map.forEachFeatureAtPixel(pixel, (feature) => {
        var region = this.regions.filter(x => x.id === +feature.getId())[0];
        this.selectedRegion = region;
        console.log(this.selectedRegion)
        // feature.setStyle(new Style({ fill: new Fill({ color: 'blue' }), stroke: new Stroke({ color: '#B6B6B6', width: 0.5 }) }));
      });
    }
  }

}
