import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/index/home.component';
import { LandingPageComponent } from './home/landing-page/landing-page.component';
import { LoginComponent } from './auth/views/login/login.component';
import { BattleComponent } from './military/views/battle/battle.component';
import { OrgMemberPrivilegesComponent } from './entities/organisations/members/org-member-privileges/org-member-privileges.component';
import { ArchiveArticleListComponent } from './social/views/archive/archive-article-list/archive-article-list.component';
import { AccomodationViewComponent } from './common/accomodation-view/accomodation-view.component';
import { EstateMarketViewComponent } from './markets/estate-market-view/estate-market-view.component';
import { TradingViewComponent } from './economy/trading/trading-view/trading-view.component';
import { NotificationsListComponent } from './common/notifications-list/notifications-list.component';
import { MessagesCreateViewComponent } from './social/messages/messages-create-view/messages-create-view.component';
import { MessagesListComponent } from './social/messages/messages-list/messages-list.component';
import { MessagesViewComponent } from './social/messages/messages-view/messages-view.component';
import { CitizenshipListComponent } from './politics/country/citizenship-list/citizenship-list.component';
import { ItemsMarketListComponent } from './markets/items-market/items-market-list/items-market-list.component';
import { TrainingComponent } from './entities/citizen/training/training.component';
import { ReferralsComponent } from './entities/citizen/referrals/referrals.component';
import { SharesComponent } from './entities/citizen/shares/shares.component';
import { FriendsComponent } from './entities/citizen/friends/friends.component';
import { CompanyCreateComponent } from './economy/company/company-create/company-create.componet';
import { IndexComponent } from './entities/citizen/index/index.component';
import { ResultsPageComponent } from './politics/elections/results-page/results-page.component';
import { VotePageComponent } from './politics/elections/vote-page/vote-page.component';
import { TravelComponent } from './common/travel/travel.component';
import { CandidatePageComponent } from './politics/elections/candidate-page/candidate-page.component';
import { InventoryComponent } from './common/inventory/inventory.component';
import { TradingListComponent } from './economy/trading/trading-list/trading-list.component';
import { ExchangeMarketViewComponent } from './markets/exchange-market/exchange-market-view/exchange-market-view.component';
import { NewspaperCreateComponent } from './social/newspaper/newspaper-create/newspaper-create.component';
import { NewspaperDetailsComponent } from './social/newspaper/newspaper-details/newspaper-details.component';
import { NewspaperSubscribersComponent } from './social/newspaper/newspaper-subscribers/newspaper-subscribers.component';
import { ParliamentProposeLawComponent } from './politics/parliament/parliament-propose-law/parliament-propose-law.component';
import { ParliamentLawComponent } from './politics/parliament/parliament-law/parliament-law.component';
import { WarsListComponent } from './military/wars/wars-list/wars-list.component';
import { MapComponent } from './backend/map/map.component';
import { ElectionsCandidateViewComponent } from './politics/elections/elections-candidate-view/elections-candidate-view.component';
import { CountrySocialViewComponent } from './politics/country/country-social-view/country-social-view.component';
import { RawsMarketComponent } from './markets/raws-market/raws-market.component';
import { ChatComponent } from './backend/chat/chat.component';
import { LegalComponent } from './backend/legal/legal.component';
import { RulesComponent } from './backend/rules/rules.component';
import { CountryAdministrationViewComponent } from './politics/country/country-administration-view/country-administration-view.component';
import { RankingComponent } from './common/ranking/ranking.component';
import { OrganisationAllViewComponent } from './entities/organisations/organisation-all-view/organisation-all-view.component';
import { CompanyAllViewComponent } from './economy/company/company-all-view/company-all-view.component';
import { JobsMarketComponent } from './markets/jobs-market/jobs-market.component';



const routes: Routes = [
  // canActivate: [AuthGuard]
  { path: 'homepage/:token', component: HomeComponent, pathMatch: 'prefix',  }, 
  
  { path: 'home', component: LandingPageComponent },
  { path: 'login', component: LoginComponent },

  { path: 'organisations/:token', component: OrganisationAllViewComponent }, 
  { path: 'organisation/permissions/:ownerToken/:userId/:orgToken', component: OrgMemberPrivilegesComponent }, 

  { path: 'companies/:token', component: CompanyAllViewComponent},
  { path: 'company/create', component: CompanyCreateComponent},

  { path: 'wars', component: WarsListComponent },
  { path: 'battle/:battleId/:token', component: BattleComponent },

  { path: 'article/archive/all', component: ArchiveArticleListComponent },

  { path: 'market/estate/:countryId/:token', component: EstateMarketViewComponent },
  { path: 'market/exchange/:token', component: ExchangeMarketViewComponent },

  { path: 'accommodation/:token', component: AccomodationViewComponent },

  { path: 'notification/:token', component: NotificationsListComponent },

  { path: 'messages/create/:token/:defaultEntity', component: MessagesCreateViewComponent },
  { path: 'messages/list/:token', component: MessagesListComponent },
  { path: 'message/:id/:token', component: MessagesViewComponent },

  { path: 'country/:countryId', component: CountrySocialViewComponent },
  { path: 'country/:countryId/citizenships/:token', component: CitizenshipListComponent },
  { path: 'country/:countryId/administration/:token', component: CountryAdministrationViewComponent },

  { path: 'market/items/:token', component: ItemsMarketListComponent },
  { path: 'market/raws/:token', component: RawsMarketComponent },
  { path: 'market/jobs/:token', component: JobsMarketComponent },

  { path: 'citizen/training', component: TrainingComponent },

  { path: 'citizen/referrals', component: ReferralsComponent },

  { path: 'citizen/:token/friends', component: FriendsComponent },

  { path: 'citizen/share', component: SharesComponent },

  { path: 'citizen/:token', component: IndexComponent },

  { path: 'elections/results', component: ResultsPageComponent },
  { path: 'elections/vote', component: VotePageComponent },

  { path: 'elections/candidate', component: ElectionsCandidateViewComponent },

  { path: 'travel/:token', component: TravelComponent },
  { path: 'travel/:token/:regionId', component: TravelComponent },

  { path: 'region/:regionId', component: TravelComponent },

  { path: 'inventory/:token', component: InventoryComponent },

  { path: 'map/:token', component: MapComponent },

  { path: 'trades/:token', component: TradingListComponent },
  { path: 'trade/:token/:tradeId', component: TradingViewComponent }, // params: :token/:tradeId

  { path: 'newspaper/create/:token', component: NewspaperCreateComponent },

  { path: 'newspaper/:newspaperId/subscribers', component: NewspaperSubscribersComponent },

  { path: 'newspaper/:newspaperId/:token', component: NewspaperDetailsComponent },

  { path: 'parliament/propose/:lawId/:token', component: ParliamentProposeLawComponent }, // params: :token/:tradeId
  { path: 'parliament/law/:lawId/:token', component: ParliamentLawComponent }, // params: :token/:tradeId
  
  { path: 'chat', component: ChatComponent },
  { path: 'legal', component: LegalComponent },
  { path: 'rules', component: RulesComponent },
  { path: 'ranking/:token', component: RankingComponent },
  { path: 'ranking', component: RankingComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
