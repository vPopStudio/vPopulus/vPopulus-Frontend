import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawsMarketComponent } from './raws-market.component';

describe('RawsMarketComponent', () => {
  let component: RawsMarketComponent;
  let fixture: ComponentFixture<RawsMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawsMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawsMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
