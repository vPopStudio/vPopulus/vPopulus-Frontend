import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-raws-market',
  templateUrl: './raws-market.component.html',
  styleUrls: ['./raws-market.component.css']
})
export class RawsMarketComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  entity;
  isLoading = false;

  page = 1;
  pageSize = 20;

  countries = [];
  selectedCountry = {};

  selectedCountryId;
  selectedItemId = 1;
  selectedCompanyId = 0;

  companies = [];

  offers = [];
  totalEntries = 0;
  canAccept = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messag: NzMessageService,
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.token = this.route.snapshot.paramMap.get('token');
    this.httpService.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(user => {
      this.selectedCountryId = user.entity.countryId;
      this.entity = user.entity;
    });
    this.httpService.get2(`/api/countries/list`, this.token).subscribe(r => {
      this.countries = r;
      this.countrySelected(this.selectedCountryId);
    });
  }

  countrySelected(value) {
    this.selectedCountry = this.countries.find(x => x.id == value);
    if (this.selectedCountryId == this.entity.countryId && this.selectedCompanyId > 0) this.canAccept = true;
    this.httpService.get2(`/api/companies/list/me/${this.selectedCountryId}?page=1&pageSize=200`, this.token).subscribe(r => {
      this.companies = r.list.filter(x => x.industryId == 2 || x.industryId == 4 || x.industryId == 6 || x.industryId == 8 || x.industryId == 14);
    });
    this.loadOffers();
  }

  companySelected(value) {
    var company = this.companies.filter(x => x.id == this.selectedCompanyId)[0];
    this.selectedItemId = company.rawIndustryId;
    this.loadOffers();
  }

  loadOffers() {
    this.isLoading = true;
    this.httpService.get2(`/api/market/raws/list/${this.selectedCountryId}/${this.selectedItemId}/${this.selectedCompanyId}?page=${this.page}&pageSize=${this.pageSize}`, this.token).subscribe(r => {
      this.offers = r.list;
      this.totalEntries = r.totalEntries;
      this.canAccept = r.canBuy;
      this.isLoading = false;
    });
  }

  buyOffer(offerId) {
    if (this.offers.filter(x => x.id == offerId)[0].amount <= 0) {
      this.messag.error("Amount needs to be higher than 0.");
      return;
    }

    this.isLoading = true;
    this.httpService.post2(`/api/market/raws/buy`, { offerId: offerId, amount: this.offers.filter(x => x.id == offerId)[0].amount, companyId: this.selectedCompanyId }, this.token).subscribe(r => {
      this.messag.success(r.message);
      this.loadOffers();
    }, error => {
      this.messag.error(error.error.message);
      this.loadOffers();
    });
  }

}
