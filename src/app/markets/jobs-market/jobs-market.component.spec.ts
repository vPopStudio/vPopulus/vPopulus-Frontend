import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JobsMarketComponent } from './jobs-market.component';

describe('JobsMarketComponent', () => {
  let component: JobsMarketComponent;
  let fixture: ComponentFixture<JobsMarketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JobsMarketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JobsMarketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
