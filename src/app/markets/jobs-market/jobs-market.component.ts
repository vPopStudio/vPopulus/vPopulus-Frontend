import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-jobs-market',
  templateUrl: './jobs-market.component.html',
  styleUrls: ['./jobs-market.component.css']
})
export class JobsMarketComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  userCountryId;
  isLoading = false;

  selectedCountryId;
  selectedSkillTypeId = 0;
  selectedQuality = 0;
  selectedSkillLevel = 15;

  skills = [
    { id: 0, name: 'All skills', icon: '' },
    { id: 1, name: 'Economic.Market.Land', icon: 'assets/img/ico/eco/skills/land.png' },
    { id: 2, name: 'Economic.Market.Manufacturing', icon: 'assets/img/ico/eco/skills/manufacture.png' },
    { id: 3, name: 'Economic.Market.Construction', icon: 'assets/img/ico/eco/skills/construction.png' },
  ];
  selectedSkillType;

  countries = [];
  selectedCountry = {};

  offers = [];
  totalEntries = 0;
  canAccept = false;

  page = 1;
  pageSize = 20;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.token = this.route.snapshot.paramMap.get('token');
    this.httpService.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(user => {
      this.selectedCountryId = user.entity.countryId;
      this.userCountryId = user.entity.countryId;
    });
    this.httpService.get2(`/api/countries/list`, this.token).subscribe(r => {
      this.countries = r;
      this.countrySelected(this.selectedCountryId, false);
      this.skillTypeSelected(0);
    });
  }

  countrySelected(value, reload = true) {
    this.selectedCountry = this.countries.find(x => x.id == value);
    if (this.selectedCountryId == this.userCountryId) this.canAccept = true;
    if (reload == true)
      this.loadOffers();
  }

  skillTypeSelected(value, reload = true) {
    this.selectedSkillType = this.skills.find(x => x.id == value);
    if (reload == true)
      this.loadOffers();
  }

  loadOffers() {
    this.isLoading = true;
    this.httpService.get2(`/api/market/jobs/list/${this.selectedCountryId}/${this.selectedSkillTypeId}/${this.selectedQuality}/${this.selectedSkillLevel}?page=${this.page}&pageSize=${this.pageSize}`, this.token).subscribe(r => {
      this.offers = r.list;
      console.log(this.offers);
      this.totalEntries = r.totalEntries;
      this.isLoading = false;
    });
  }

  acceptOffer(id) {
    this.isLoading = true;
    this.httpService.post2(`/api/market/jobs/accept`, { offerId: id }, this.token).subscribe(r => {
      if (r.isSuccessful == true) {
        this.messageService.success(r.message);
      } else {
        this.messageService.error(r.message);
      }
      this.loadOffers();
    });
  }

}
