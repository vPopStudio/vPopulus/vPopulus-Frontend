import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { NzMessageService } from 'ng-zorro-antd';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-exchange-market-view',
  templateUrl: './exchange-market-view.component.html',
  styleUrls: ['./exchange-market-view.component.css']
})
export class ExchangeMarketViewComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  page = 1;
  isLoading = [false, false];

  countries: any[] = [];
  buyCurrencyId;
  buyCurrency: any = { id: 0 };
  buyBalance = 0;

  sellCurrencyId;
  sellCurrency: any = { id: 0 };
  sellBalance = 0;

  finances: any[] = [];

  offers: any[] = [];
  myOffers: any[] = [];
  totalEntries = 0;
  myOffersTotalEntries = 0;

  newOffer: any = { rate: 0, amount: 0 };


  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.isLoading[0] = true;
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
      this.loadFinances();
      this.httpClient.get2(`/api/countries/list`).subscribe(r => {
        this.countries = r.reverse();
        this.countries.push({ currencyId: 1, currencyName: "Gold", name: "Gold", flagLocation: ["assets/img/fla/s/vpopulus.png", "assets/img/fla/xl/vpopulus.png"] })
        this.countries = this.countries.reverse();
        this.httpClient.get2(`/api/auth/getdata/${this.token}`).subscribe(user => {
          this.buyCurrencyId = 1;
          this.sellCurrencyId = user.entity.countryId;
          this.buyCurrencySelected(0);
          this.sellCurrencySelected(1);
        });
      });
    });
    this.loadMyOffers();
  }

  loadFinances() {
    this.httpClient.get2(`/api/finances`, this.token).subscribe(r => {
      this.finances = r;
    });
  }

  loadOffers() {
    if (this.buyCurrencyId === undefined || this.sellCurrencyId === undefined)
      return;
    this.isLoading[0] = true;

    this.httpClient.get2(`/api/market/exchange/offers/${this.sellCurrencyId}/${this.buyCurrencyId}?page=${this.page}`, this.token).subscribe(r => {
      this.offers = r.list;
      this.totalEntries = r.totalEntries;
      this.isLoading[0] = false;
    });
  }

  loadMyOffers() {
    this.isLoading[1] = true;

    this.httpClient.get2(`/api/market/exchange/offers?page=${this.page}`, this.token).subscribe(r => {
      this.myOffers = r.list;
      this.myOffersTotalEntries = r.totalEntries;
      this.isLoading[1] = false;
    });
  }

  buyOffer(id) {
    var offer = this.offers.filter(x => x.id == id)[0];
    if (offer === undefined || offer.amountToBuy < 0.01) {
      this.message.error('Amount needs to be at least 0.01.');
      return;
    }

    this.isLoading[0] = true;
    this.httpClient.post2(`/api/market/exchange/accept`, { offerId: id, amount: offer.amountToBuy }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadFinances();
      this.loadOffers();
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[0] = false;
    });
  }

  buyCurrencySelected(value) {
    this.buyCurrency = this.countries.filter(x => x.currencyId == this.buyCurrencyId)[0];
    this.buyBalance = this.finances.filter(x => x.currencyId == this.buyCurrencyId)[0] === undefined ? 0 : this.finances.filter(x => x.currencyId == this.buyCurrencyId)[0].amount;
    if (value > 0)
      this.loadOffers();
  }

  sellCurrencySelected(value) {
    this.sellCurrency = this.countries.filter(x => x.currencyId == this.sellCurrencyId)[0];
    this.sellBalance = this.finances.filter(x => x.currencyId == this.sellCurrencyId)[0] === undefined ? 0 : this.finances.filter(x => x.currencyId == this.sellCurrencyId)[0].amount;
    if (value > 0)
      this.loadOffers();
  }

  switchCurrencies() {
    var tmpCurrId = this.sellCurrencyId;
    this.sellCurrencyId = this.buyCurrencyId;
    this.buyCurrencyId = tmpCurrId;
    this.buyCurrencySelected(0);
    this.sellCurrencySelected(0);
    this.loadOffers();
  }

  addOffer() {
    this.isLoading[1] = true;
    this.httpClient.post2(`/api/market/exchange/`, { buyId: this.sellCurrencyId, sellId: this.buyCurrencyId, rate: this.newOffer.rate, amount: this.newOffer.amount }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadMyOffers();
      this.loadOffers();
      this.loadFinances();
      this.newOffer = { rate: 0, amount: 0 }
      this.isLoading[1] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
    });
  }

  removeOffer(id) {
    this.isLoading[1] = true;
    this.httpClient.delete(`/api/market/exchange/${id}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadMyOffers();
      this.loadFinances();
      this.isLoading[1] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
    });
  }

}
