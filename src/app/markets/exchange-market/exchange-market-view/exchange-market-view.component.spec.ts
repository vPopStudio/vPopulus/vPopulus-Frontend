import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ExchangeMarketViewComponent } from './exchange-market-view.component';

describe('ExchangeMarketViewComponent', () => {
  let component: ExchangeMarketViewComponent;
  let fixture: ComponentFixture<ExchangeMarketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ExchangeMarketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ExchangeMarketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
