import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';


@Component({
  selector: 'app-estate-market-view',
  templateUrl: './estate-market-view.component.html',
  styleUrls: ['./estate-market-view.component.css']
})
export class EstateMarketViewComponent implements OnInit {
  hostUrl = environment.hostUrl;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  countries: any[] = [];
  selectedCountryId;
  selectedCountry: any = { id: 0 };

  constructionTypes: any[] = [];
  selectedConstructionType = [];

  offers: any[] = [];
  page: number = 1;
  pageSize: number = 10;
  totalEntries = 0;

  isLoading: any[] = [false, false];

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.token = params.get('token');
      this.httpClient.get2(`/api/countries/list`, this.token).subscribe(r => {
        this.countries = r;
        this.selectedCountryId = params.get('countryId');
        this.countrySelected(params.get('countryId'));
      });
    });
    this.httpClient.get2(`/api/market/estate/GetConstructionTypes`, this.token).subscribe(r => {
      this.constructionTypes = r;
      this.selectedConstructionType = ['1', '10'];
      this.loadOffers();
    });
  }

  countrySelected(value) {
    this.selectedCountry = this.countries.find(x => x.id == value);
    this.loadOffers();
  }

  loadOffers() {
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/market/estate/GetList?countryId=${this.selectedCountry.id}&constructionType=${this.selectedConstructionType}&page=${this.page}&pageSize=${this.pageSize}`, this.token).subscribe(r => {
      this.offers = r.offers;
      this.totalEntries = r.totalOffers;
      this.isLoading[0] = false;
    });
  }

  buyOffer(offerId) {
    this.isLoading[0] = true;
    this.httpClient.post2(`/api/market/estate/BuyOffer`, { offerId: offerId }, this.token).subscribe(r => {
      this.translate.get(r.message).subscribe(t => {
        this.message.success(t);
        this.loadOffers();
      })
    }, error => {
      this.translate.get(error.error.message).subscribe(t => {
        this.message.error(t);
        this.loadOffers();
      })
    });
  }




}
