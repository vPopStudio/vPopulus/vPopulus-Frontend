import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstateMarketViewComponent } from './estate-market-view.component';

describe('EstateMarketViewComponent', () => {
  let component: EstateMarketViewComponent;
  let fixture: ComponentFixture<EstateMarketViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstateMarketViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstateMarketViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
