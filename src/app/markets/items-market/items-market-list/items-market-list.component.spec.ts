import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemsMarketListComponent } from './items-market-list.component';

describe('ItemsMarketListComponent', () => {
  let component: ItemsMarketListComponent;
  let fixture: ComponentFixture<ItemsMarketListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemsMarketListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemsMarketListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
