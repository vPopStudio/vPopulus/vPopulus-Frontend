import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { userInfo } from 'os';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-items-market-list',
  templateUrl: './items-market-list.component.html',
  styleUrls: ['./items-market-list.component.css']
})
export class ItemsMarketListComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  userCountryId;
  isLoading = false;

  page = 1;
  pageSize = 20;

  countries = [];
  selectedCountry = {};

  selectedCountryId;
  selectedItemId = 2;
  selectedQuality = 0;

  offers = [];
  totalEntries = 0;
  canAccept = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messag: NzMessageService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.token = this.route.snapshot.paramMap.get('token');
    this.httpService.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(user => {
      this.selectedCountryId = user.entity.countryId;
      this.userCountryId = user.entity.countryId;
    });
    this.httpService.get2(`/api/countries/list`, this.token).subscribe(r => {
      this.countries = r;
      this.countrySelected(this.selectedCountryId);
    });
  }

  countrySelected(value) {
    this.selectedCountry = this.countries.find(x => x.id == value);
    if (this.selectedCountryId == this.userCountryId) this.canAccept = true;
    this.loadOffers();
  }

  loadOffers() {
    this.isLoading = true;
    this.httpService.get2(`/api/market/items/list/${this.selectedCountryId}/${this.selectedItemId}/${this.selectedQuality}?page=${this.page}&pageSize=${this.pageSize}`).subscribe(r => {
      this.offers = r.list;
      this.totalEntries = r.totalEntries;
      this.isLoading = false;
    });
  }

  buyOffer(offerId) {
    if (this.offers.filter(x => x.id == offerId)[0].amount <= 0) {
      this.messag.error("Amount needs to be higher than 0.");
      return;
    }

    this.isLoading = true;
    this.httpService.post2(`/api/market/items/buy`, { offerId: offerId, amount: this.offers.filter(x => x.id == offerId)[0].amount }, this.token).subscribe(r => {
      this.messag.success(r.message);
      this.loadOffers();
    }, error => {
      this.messag.error(error.error.message);
      this.loadOffers();
    });
  }

}
