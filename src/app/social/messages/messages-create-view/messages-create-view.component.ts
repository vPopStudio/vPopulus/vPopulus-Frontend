import { Component, OnInit } from '@angular/core';
import { MentionOnSearchTypes, NzMessageService } from 'ng-zorro-antd';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-messages-create-view',
  templateUrl: './messages-create-view.component.html',
  styleUrls: ['./messages-create-view.component.css']
})
export class MessagesCreateViewComponent implements OnInit {
  title = "";
  content = "";
  users: string = "";

  suggestions: string[] = [];
  token;

  isLoading = [false, false];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private router: Router,
    private message: NzMessageService
  ) { }

  onSearchChange({ value }: MentionOnSearchTypes): void {
    this.isLoading[1] = true;
    this.httpService.get2(`/api/entities/search/${value}`).subscribe(r => {
      this.suggestions = r.filter(x => x.entityTypeName.toLowerCase().includes('citizen') || x.entityTypeName.toLowerCase().includes('organisation') || x.entityTypeName.toLowerCase().includes('army') || x.entityTypeName.toLowerCase().includes('party'))
        .map(x => x.name);
      this.isLoading[1] = false;
    });
  }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.users = `@${this.route.snapshot.paramMap.get("defaultEntity")} `
  }

  submit() {
    if (this.title === "" || this.content === "") {
      this.message.error("Title or message is missing!")
      return;
    }

    if (this.users === "") {
      this.message.error("Please select who do you want to send message to!")
      return;
    }

    this.isLoading[0] = true;
    this.httpService.post2(`/api/messages/create`, { title: this.title, content: this.content, users: this.users.trim().split('@').filter(x => x !== "").map(x => x.trim()) }, this.token).subscribe(r => {
      this.isLoading[0] = false;
      window.location.href = `${environment.hostUrl}/message/${r.returnInt1}`;
      // this.router.navigateByUrl(`/message/${r.returnInt1}/${this.token}`);
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[0] = false;
    });
  }

}
