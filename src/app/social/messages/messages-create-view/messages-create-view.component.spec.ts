import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MessagesCreateViewComponent } from './messages-create-view.component';

describe('MessagesCreateViewComponent', () => {
  let component: MessagesCreateViewComponent;
  let fixture: ComponentFixture<MessagesCreateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MessagesCreateViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MessagesCreateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
