import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-messages-list',
  templateUrl: './messages-list.component.html',
  styleUrls: ['./messages-list.component.css']
})
export class MessagesListComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  page = 1;
  isLoading = [true];

  messages = [];
  totalEntries = 0;
  allChecked = false;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadData();
  }

  loadData() {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/messages/getlist/${this.page}`, this.token).subscribe(r => {
      this.messages = r.list;
      this.messages.forEach(message => {
        var split = message.createdOn.replace('T', ' ').split(' ');
        message.createdOn = `${split[0]} ${split[1].split('.')[0]}`;
      });

      this.totalEntries = r.totalEntries;
      this.allChecked = false;
      this.isLoading[0] = false;
    });
  }

  checkAll() {
    for (var item of this.messages) {
      item.isChecked = !item.isChecked;
    };
  }

  remove() {
    this.isLoading[0] = true;
    this.httpService.post2(`/api/messages/remove`, { ids: this.messages.filter(x => x.isChecked === true).map(x => x.id).join(',') }, this.token).subscribe(r => {
      this.loadData();
      this.message.success(r.message);
    });
  }

}
