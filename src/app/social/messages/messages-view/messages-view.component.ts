import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { MentionOnSearchTypes, NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-messages-view',
  templateUrl: './messages-view.component.html',
  styleUrls: ['./messages-view.component.css']
})
export class MessagesViewComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  messageId;
  page = 1;
  pageSize = 6;
  isLoading = [false, false, false];

  content = "";
  users: string = "";
  suggestions: string[] = [];
  message: any = {};

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private nzMessage: NzMessageService
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.messageId = this.route.snapshot.paramMap.get("id");
    this.loadData();
  }

  loadData() {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/messages/GetById/${this.messageId}/${this.page}/${this.pageSize}`, this.token).subscribe(r => {
      this.message = r;
      this.isLoading[0] = false;
    });
  }

  removeUser(id) {
    this.isLoading[2] = true;
    this.httpService.post2(`/api/message-members/Remove`, { id: this.messageId, memberEntryId: id }, this.token).subscribe(r => {
      this.nzMessage.success(r.message);
      this.loadData();
      this.isLoading[2] = false;
    }, error => {
      this.nzMessage.success(error.error.message);
      this.isLoading[2] = false;
    });
  }

  addUser() {
    this.isLoading[2] = true;
    this.httpService.post2(`/api/message-members/Add`, { id: this.messageId, users: this.users.trim().split('@').filter(x => x !== "").map(x => x.trim()) }, this.token).subscribe(r => {
      this.nzMessage.success(r.message);
      this.loadData();
      this.users = "";
      this.isLoading[2] = false;
    }, error => {
      this.nzMessage.success(error.error.message);
      this.isLoading[2] = false;
    });
  }

  postMessage() {
    if (this.content === "") {
      this.nzMessage.error("Message is missing!")
      return;
    }

    this.isLoading[0] = true;
    this.httpService.post2(`/api/messages/reply`, { id: this.messageId, content: this.content }, this.token).subscribe(r => {
      this.content = "";
      this.loadData();
    });

  }

  onSearchChange({ value }: MentionOnSearchTypes): void {
    this.isLoading[1] = true;
    this.httpService.get2(`/api/entities/search/${value}`).subscribe(r => {
      this.suggestions = r.filter(x => x.entityTypeName.toLowerCase().includes('citizen') || x.entityTypeName.toLowerCase().includes('organisation') || x.entityTypeName.toLowerCase().includes('army') || x.entityTypeName.toLowerCase().includes('party'))
        .map(x => x.name);
      this.isLoading[1] = false;
    });
  }

}
