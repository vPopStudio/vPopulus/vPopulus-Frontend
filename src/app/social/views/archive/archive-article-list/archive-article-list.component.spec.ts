import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ArchiveArticleListComponent } from './archive-article-list.component';

describe('ArchiveArticleListComponent', () => {
  let component: ArchiveArticleListComponent;
  let fixture: ComponentFixture<ArchiveArticleListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ArchiveArticleListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArchiveArticleListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
