import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-archive-article-list',
  templateUrl: './archive-article-list.component.html',
  styleUrls: ['./archive-article-list.component.css']
})
export class ArchiveArticleListComponent implements OnInit {
  hostUrl: any;

  partOfTitle: string = "";
  isLoading: boolean = false;
  
  articlesList: any[] = [];

  totalEntries: number =0;
  page: number = 1;

  constructor(
    private httpService: GenericHttpService,
  ) { }

  ngOnInit() {
    this.hostUrl = environment.hostUrl;

    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/article/archive/getall?page=${this.page}&pageSize=15&partOfTitle=${this.partOfTitle}`).subscribe(r => {
      this.articlesList = r.list;
      this.totalEntries = r.totalEntries;
      this.isLoading = false;
    });
  }

  pageChange(page) {
    this.page = page;
    this.loadData();
  }

}
