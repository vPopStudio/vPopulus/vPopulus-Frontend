import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute, Router } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-newspaper-create',
  templateUrl: './newspaper-create.component.html',
  styleUrls: ['./newspaper-create.component.css']
})
export class NewspaperCreateComponent implements OnInit {
  hostUrl = environment.hostUrl;

  name: string = '';

  entity: any;
  gold: any;

  isLoading = false;

  xpRequirement: number = 8; // TODO: load from config

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private translate: TranslateService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.isLoading = true;
    this.getEntityLocation();
    this.getFinanceGold();
  }

  create() {
    this.isLoading = true;
    this.httpClient.post2(`/api/newspapers`,
      {
        name: this.name
      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
          this.router.navigateByUrl('/newspaper/' + t.returnInt1);
          this.isLoading = false;
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

  getEntityLocation() {
    this.httpClient.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(r => {
      this.entity = r.entity;
    });
  }


  getFinanceGold() {
    this.httpClient.get2(`/api/finances`, this.token).subscribe(account => {
      //console.log(account);
      this.gold = account.find(x => x.currencyId === 1);
      this.isLoading = false;
    });
  }
}
