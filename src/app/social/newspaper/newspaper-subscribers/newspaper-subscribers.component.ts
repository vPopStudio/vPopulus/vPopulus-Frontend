import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-newspaper-subscribers',
  templateUrl: './newspaper-subscribers.component.html',
  styleUrls: ['./newspaper-subscribers.component.css']
})
export class NewspaperSubscribersComponent implements OnInit {
  hostUrl = environment.hostUrl;

  newspaperId: number = 0;

  subscribers: any[] = [];
  totalSubscribers: number = 0;

  page: number = 1;
  pageSize: number = 15;

  isLoading: any[] = [false];

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }


  ngOnInit() {
    this.newspaperId = +this.route.snapshot.paramMap.get("newspaperId"); // + converts to number

    this.getSubscribers();
  }

  getSubscribers() {
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/newspapers/subscribers/${this.newspaperId}?page=${this.page}&pageSize=${this.pageSize}`).subscribe(r => {
      this.subscribers = r.list;
      this.totalSubscribers = r.totalEntries;
      console.log(this.subscribers);
      this.isLoading[0] = false;
    });
  }
}
