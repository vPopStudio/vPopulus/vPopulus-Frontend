import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewspaperSubscribersComponent } from './newspaper-subscribers.component';

describe('NewspaperSubscribersComponent', () => {
  let component: NewspaperSubscribersComponent;
  let fixture: ComponentFixture<NewspaperSubscribersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewspaperSubscribersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewspaperSubscribersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
