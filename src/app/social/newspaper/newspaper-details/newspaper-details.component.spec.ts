import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NewspaperDetailsComponent } from './newspaper-details.component';

describe('NewspaperDetailsComponent', () => {
  let component: NewspaperDetailsComponent;
  let fixture: ComponentFixture<NewspaperDetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NewspaperDetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NewspaperDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
