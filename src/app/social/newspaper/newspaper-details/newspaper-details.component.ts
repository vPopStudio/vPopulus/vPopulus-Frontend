import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { debug } from 'util';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-newspaper-details',
  templateUrl: './newspaper-details.component.html',
  styleUrls: ['./newspaper-details.component.css']
})
export class NewspaperDetailsComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token = ''; // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN

  entity: any;

  newspaper: any;

  subscribers: any[] = [];
  totalSubscribers: number = 0;

  articles: any[] = [];
  totalArticles: number = 0;

  page: number = 1;
  pageSize: number = 15;

  isLoading: any[] = [false, false, false];

  // Model.Newspaper.Subscribers.Find(x => x.CitizenId == Model.EntityId) == null
  isCitizenSubscribed: boolean = false;
  isOwner: boolean = false;

  newspaperId: number = 0;

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private translate: TranslateService,
  ) { }


  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.newspaperId = +this.route.snapshot.paramMap.get("newspaperId"); // + converts to number

    this.isLoading[0] = true;
    this.getEntity();
    this.getNewspaper();
    this.isEntityOwner();

    this.getSubscribers();
    this.isEntitySubscribed();

    this.getArticles();

    this.isLoading[0] = false;

    console.log(this.isCitizenSubscribed);
    console.log(this.isOwner);
  }

  getEntity() {
    this.httpClient.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(r => {
      this.entity = r;
    });
  }

  getNewspaper() {
    this.httpClient.get2(`/api/newspapers/${this.newspaperId}`, this.token).subscribe(r => {
      this.newspaper = r;
    });

  }

  getSubscribers() {
    this.isLoading[2] = true;
    this.httpClient.get2(`/api/newspapers/subscribers/${this.newspaperId}?page=${this.page}&pageSize=${this.pageSize}`, this.token).subscribe(r => {
      this.subscribers = r.list;
      this.totalSubscribers = r.totalEntries;

      this.isLoading[2] = false;
    });
  }

  getArticles() {
    this.isLoading[1] = true;
    this.httpClient.get2(`/api/articles/newspapers/${this.newspaperId}?page=${this.page}&pageSize=${this.pageSize}`, this.token).subscribe(r => {
      this.articles = r.list;
      this.totalArticles = r.totalEntries;
      this.isLoading[1] = false;
    });
  }

  isEntitySubscribed() {
    //if (this.subscribers.some(sub => sub.entity.entityId == this.entity.entityId) == true) {
    //  this.isCitizenSubscribed = true;
    //}
    //else {
    //  this.isCitizenSubscribed = false;
    //}
    this.isCitizenSubscribed = true;
  }

  isEntityOwner() {
    //if (this.newspaper.owner.entityId == this.entity.entityId) {
    //  this.isOwner = true;
    //}
    //else {
    //  this.isOwner = false;
    //}
    this.isOwner = true;
  }

  subscribe(newspaperId) {
    this.httpClient.post2(``,
      {

      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
        })
      });
  }

  unsubscribe(newspaperId) {
    this.httpClient.post2(``,
      {

      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
        })
      });
  }

  deleteArticle(selectedArticleId) {
    this.httpClient.post2(``,
      {

      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
        })
      });
  }

  cancel() { // delete confirmation box cancel
    // nothing
    this.message.info('Canceled');
  }
}
