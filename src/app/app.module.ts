import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AngularOpenlayersModule } from "ngx-openlayers";
import { SweetAlert2Module } from '@sweetalert2/ngx-sweetalert2';

import { AppComponent } from './app.component';
import { GuestHomepageComponent } from './home/guest-homepage/guest-homepage.component';
import { HomeComponent } from './home/index/home.component';
import { LandingPageComponent } from './home/landing-page/landing-page.component';
import { LoginComponent } from './auth/views/login/login.component';

// import ngx-translate and the http loader
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { CustomTranslateLoader } from './common/services/custom-translate-loader.service';
import { HttpClientModule } from '@angular/common/http';
import { HttpClient } from '@angular/common/http';
import { MapComponent } from './backend/map/map.component';
import { BattleComponent } from './military/views/battle/battle.component';

import { CookieService } from 'ngx-cookie-service';

// Ant design zorro
import { NgZorroAntdModule, NZ_I18N, en_US, NzModalRef } from 'ng-zorro-antd';
/** config angular i18n **/
import en from '@angular/common/locales/en';
import { OrgMemberPrivilegesComponent } from './entities/organisations/members/org-member-privileges/org-member-privileges.component';
import { ArchiveArticleListComponent } from './social/views/archive/archive-article-list/archive-article-list.component';
import { FormsModule } from '@angular/forms';
import { EstateMarketViewComponent } from './markets/estate-market-view/estate-market-view.component';
import { InventoryComponent } from './common/inventory/inventory.component';
import { AccomodationViewComponent } from './common/accomodation-view/accomodation-view.component';
import { AccommodationManagementComponent } from './common/accomodation-view/accommodation-management/accommodation-management.component';
import { TradingViewComponent } from './economy/trading/trading-view/trading-view.component';
import { TradingListComponent } from './economy/trading/trading-list/trading-list.component';
import { NotificationsListComponent } from './common/notifications-list/notifications-list.component';
import { MessagesCreateViewComponent } from './social/messages/messages-create-view/messages-create-view.component';
import { MessagesListComponent } from './social/messages/messages-list/messages-list.component';
import { MessagesViewComponent } from './social/messages/messages-view/messages-view.component';
import { CitizenshipListComponent } from './politics/country/citizenship-list/citizenship-list.component';
import { ItemsMarketListComponent } from './markets/items-market/items-market-list/items-market-list.component';
import { ReferralsComponent } from './entities/citizen/referrals/referrals.component';
import { SharesComponent } from './entities/citizen/shares/shares.component';
import { TrainingComponent } from './entities/citizen/training/training.component';
import { CompanyCreateComponent } from './economy/company/company-create/company-create.componet';
import { IndexComponent } from './entities/citizen/index/index.component';
import { FriendsComponent } from './entities/citizen/friends/friends.component';
import { VotePageComponent } from './politics/elections/vote-page/vote-page.component';
import { CandidatePageComponent } from './politics/elections/candidate-page/candidate-page.component';
import { ResultsPageComponent } from './politics/elections/results-page/results-page.component';
import { TravelComponent } from './common/travel/travel.component';
import { QualityStarsPipe } from './util/quality-stars.pipe';
import { TradingViewInventoryComponent } from './economy/trading/trading-view-inventory/trading-view-inventory.component';
import { TradingViewAccountComponent } from './economy/trading/trading-view-account/trading-view-account.component';
import { RemoveUnderscorePipe } from './util/remove-underscore.pipe';
import { FilterByEntityPipe } from './util/filter.pipe';
import { BattlesComponent } from './home/components/battles/battles.component';
import { ShoutsComponent } from './home/components/shouts/shouts.component';
import { ArticlesComponent } from './home/components/articles/articles.component';
import { EventsComponent } from './home/components/events/events.component';
import { ExchangeMarketViewComponent } from './markets/exchange-market/exchange-market-view/exchange-market-view.component';
import { NewspaperCreateComponent } from './social/newspaper/newspaper-create/newspaper-create.component';
import { NewspaperSubscribersComponent } from './social/newspaper/newspaper-subscribers/newspaper-subscribers.component';
import { NewspaperDetailsComponent } from './social/newspaper/newspaper-details/newspaper-details.component';
import { ParliamentProposeLawComponent } from './politics/parliament/parliament-propose-law/parliament-propose-law.component';
import { LawMppComponent } from './politics/parliament/laws/law-mpp/law-mpp.component';
import { LawImpeachComponent } from './politics/parliament/laws/law-impeach/law-impeach.component';
import { LawWelcomeMessageComponent } from './politics/parliament/laws/law-welcome-message/law-welcome-message.component';
import { LawDonateComponent } from './politics/parliament/laws/law-donate/law-donate.component';
import { LawMinimumWageComponent } from './politics/parliament/laws/law-minimum-wage/law-minimum-wage.component';
import { LawProposePeaceComponent } from './politics/parliament/laws/law-propose-peace/law-propose-peace.component';
import { LawDeclareWarComponent } from './politics/parliament/laws/law-declare-war/law-declare-war.component';
import { LawTradeEmbargoComponent } from './politics/parliament/laws/law-trade-embargo/law-trade-embargo.component';
import { LawChangeTaxesComponent } from './politics/parliament/laws/law-change-taxes/law-change-taxes.component';
import { LawCitizenFeeComponent } from './politics/parliament/laws/law-citizen-fee/law-citizen-fee.component';
import { LawIssueMoneyComponent } from './politics/parliament/laws/law-issue-money/law-issue-money.component';
import { LawAllianceComponent } from './politics/parliament/laws/law-alliance/law-alliance.component';
import { LawNatOrgComponent } from './politics/parliament/laws/law-nat-org/law-nat-org.component';
import { LawMoveCapitalComponent } from './politics/parliament/laws/law-move-capital/law-move-capital.component';
import { LawReferendumComponent } from './politics/parliament/laws/law-referendum/law-referendum.component';
import { ParliamentLawComponent } from './politics/parliament/parliament-law/parliament-law.component';
import { StatSalariesFeesComponent } from './politics/country/stat-salaries-fees/stat-salaries-fees.component';
import { StatAlliesComponent } from './politics/country/stat-allies/stat-allies.component';
import { StatEmbargosComponent } from './politics/country/stat-embargos/stat-embargos.component';
import { WarsListComponent } from './military/wars/wars-list/wars-list.component';
import { ElectionsCandidateViewComponent } from './politics/elections/elections-candidate-view/elections-candidate-view.component';
import { RulesComponent } from './backend/rules/rules.component';
import { StoreComponent } from './backend/store/store.component';
import { SupportAllComponent } from './backend/support/support-all/support-all.component';
import { SupportViewComponent } from './backend/support/support-view/support-view.component';
import { ChatComponent } from './backend/chat/chat.component';
import { RankingComponent } from './common/ranking/ranking.component';
import { CountrySocialViewComponent } from './politics/country/country-social-view/country-social-view.component';
import { CountryEconomyViewComponent } from './politics/country/country-economy-view/country-economy-view.component';
import { CountryMilitaryViewComponent } from './politics/country/country-military-view/country-military-view.component';
import { CountryPoliticsViewComponent } from './politics/country/country-politics-view/country-politics-view.component';
import { CountryAdministrationViewComponent } from './politics/country/country-administration-view/country-administration-view.component';
import { CountryTopbarComponent } from './politics/country/country-topbar/country-topbar.component';
import { RawsMarketComponent } from './markets/raws-market/raws-market.component';
import { LegalComponent } from './backend/legal/legal.component';
import { OrganisationViewComponent } from './entities/organisations/organisation-view/organisation-view.component';
import { OrganisationAllViewComponent } from './entities/organisations/organisation-all-view/organisation-all-view.component';
import { CompanyAllViewComponent } from './economy/company/company-all-view/company-all-view.component';
import { JobsMarketComponent } from './markets/jobs-market/jobs-market.component';
import { TimeLeftPipe } from './util/time-left.pipe';
import { HeaderComponent } from './layout/header/header.component';
import { NavbarComponent } from './layout/navbar/navbar.component';
import { SidebarComponent } from './layout/sidebar/sidebar.component';
import { FooterComponent } from './layout/footer/footer.component';
import { TimeAgoPipe } from './util/time-ago.pipe';
import { SearchViewComponent } from './common/search-view/search-view.component';
import { TranslationFixPipe } from './util/translationFix.pipe';
import { TranslateParamsPipe } from './util/translateParams.pipe';
import { MomentModule } from 'ngx-moment';
import { RegionViewComponent } from './politics/region/region-view/region-view.component';

@NgModule({
  declarations: [
    AppComponent,
    GuestHomepageComponent,
    HomeComponent,
    LandingPageComponent,
    LoginComponent,
    MapComponent,
    BattleComponent,
    OrgMemberPrivilegesComponent,
    ArchiveArticleListComponent,
    EstateMarketViewComponent,
    InventoryComponent,
    AccomodationViewComponent,
    AccommodationManagementComponent,
    TradingViewComponent,
    TradingListComponent,
    NotificationsListComponent,
    MessagesCreateViewComponent,
    MessagesListComponent,
    MessagesViewComponent,
    CitizenshipListComponent,
    ItemsMarketListComponent,
    ReferralsComponent,
    SharesComponent,
    TrainingComponent,
    CompanyCreateComponent,
    IndexComponent,
    FriendsComponent,
    VotePageComponent,
    CandidatePageComponent,
    ResultsPageComponent,
    TravelComponent,
    QualityStarsPipe,
    TradingViewInventoryComponent,
    TradingViewAccountComponent,
    RemoveUnderscorePipe,
    TimeLeftPipe,
    TranslateParamsPipe,
    TimeAgoPipe,
    FilterByEntityPipe,
    TranslationFixPipe,
    BattlesComponent,
    ShoutsComponent,
    ArticlesComponent,
    EventsComponent,
    ExchangeMarketViewComponent,
    NewspaperCreateComponent,
    NewspaperSubscribersComponent,
    NewspaperDetailsComponent,
    ParliamentProposeLawComponent,
    LawMppComponent,
    LawImpeachComponent,
    LawWelcomeMessageComponent,
    LawDonateComponent,
    LawMinimumWageComponent,
    LawProposePeaceComponent,
    LawDeclareWarComponent,
    LawTradeEmbargoComponent,
    LawChangeTaxesComponent,
    LawCitizenFeeComponent,
    LawIssueMoneyComponent,
    LawAllianceComponent,
    LawNatOrgComponent,
    LawMoveCapitalComponent,
    LawReferendumComponent,
    ParliamentLawComponent,
    StatSalariesFeesComponent,
    StatAlliesComponent,
    StatEmbargosComponent,
    WarsListComponent,
    ElectionsCandidateViewComponent,
    RulesComponent,
    StoreComponent,
    SupportAllComponent,
    SupportViewComponent,
    ChatComponent,
    RankingComponent,
    CountrySocialViewComponent,
    CountryEconomyViewComponent,
    CountryMilitaryViewComponent,
    CountryPoliticsViewComponent,
    CountryAdministrationViewComponent,
    CountryTopbarComponent,
    RawsMarketComponent,
    LegalComponent,
    OrganisationViewComponent,
    OrganisationAllViewComponent,
    CompanyAllViewComponent,
    JobsMarketComponent,
    HeaderComponent, SidebarComponent, NavbarComponent, FooterComponent, SearchViewComponent, RegionViewComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    MomentModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CustomTranslateLoader,
        deps: [HttpClient]
      }
    }),
    AngularOpenlayersModule,
    NgZorroAntdModule,
    SweetAlert2Module.forRoot(),
  ],
  providers: [
    { provide: NZ_I18N, useValue: en_US },
    CookieService, TimeLeftPipe, TimeAgoPipe, TranslationFixPipe, TranslateParamsPipe
  ],
  entryComponents: [AccommodationManagementComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
