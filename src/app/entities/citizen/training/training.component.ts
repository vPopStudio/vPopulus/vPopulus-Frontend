import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-training',
  templateUrl: './training.component.html',
  styleUrls: ['./training.component.css']
})
export class TrainingComponent implements OnInit {
  hostUrl = environment.hostUrl;

  TrainingResult = 
    { Item1: 2.64537, Item2: 6 }
  ;

  CitizenStats = 
    { Strength: 5.124 }
  ;

  pointsNeeded = 3.789;


  constructor() { }

  ngOnInit() {
  }

  round(var1, var2) {
    return Math.round(var1);
  }

}
