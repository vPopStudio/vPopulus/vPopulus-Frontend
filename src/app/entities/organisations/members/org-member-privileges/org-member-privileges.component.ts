import { Component, OnInit, ViewChild } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { NzTreeNode, NzTreeNodeOptions, NzFormatEmitEvent, NzTreeComponent, NzMessageService } from 'ng-zorro-antd';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-org-member-privileges',
  templateUrl: './org-member-privileges.component.html',
  styleUrls: ['./org-member-privileges.component.css']
})
export class OrgMemberPrivilegesComponent implements OnInit {
  hostUrl = environment.hostUrl;

  @ViewChild('nzTreeComponent', { static: false }) nzTreeComponent: NzTreeComponent;

  ownerToken: string;
  userId: string;
  orgToken: string;

  name = '';

  nodes: NzTreeNodeOptions[] = [
    {
      title: 'All', key: 'all', children: [], expanded: true
    }
  ];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
  ) {
    this.userId = this.route.snapshot.paramMap.get("userId");
    this.orgToken = this.route.snapshot.paramMap.get("orgToken");
    this.ownerToken = this.route.snapshot.paramMap.get("ownerToken");
  }

  ngAfterViewInit(): void {
    var root = this.nzTreeComponent.getTreeNodeByKey('all');
    this.httpService.get(`/api/organisationMember/GetCitizenPrivileges/${this.ownerToken}/${this.userId}/${this.orgToken}`).subscribe(r => {
      var newChildrens = [];
      this.name = r.name;
      r.privileges.forEach(level0 => {
        var newNode = { title: level0.description, key: level0.name, expanded: true, checked: level0.isAllowed, children: [] };
        level0.sublist.forEach(level1 => {
          var key = level1.name;
          if (level1.intValue1 > 0)
            key = `${level1.name}-${level1.intValue1}`;
          newNode.children.push({ title: level1.description, key: key, expanded: true, checked: level1.isAllowed, isLeaf: true });
        });
        newChildrens.push(newNode);
      });
      root.addChildren(newChildrens)
    });
  }

  ngOnInit() { }

  updatePrivileges(event: NzFormatEmitEvent) {
    var checkedKeys = [];
    var root = this.nzTreeComponent.getTreeNodeByKey('all');
    if (root.isChecked) checkedKeys.push("all");
    root.children.forEach(level1 => {
      if (level1.isChecked) checkedKeys.push(level1.key);
      level1.children.forEach(level2 => {
        if (level2.isChecked) checkedKeys.push(level2.key);
      });
    });
    this.httpService.post(`/api/organisationMember/UpdatePrivileges`, { ownerToken: this.ownerToken, orgToken: this.orgToken, userId: this.userId, keys: checkedKeys }).subscribe(r => {
      console.log(checkedKeys)
      this.message.success('Saved');
    });
  }
}
