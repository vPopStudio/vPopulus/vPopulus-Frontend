import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrgMemberPrivilegesComponent } from './org-member-privileges.component';

describe('OrgMemberPrivilegesComponent', () => {
  let component: OrgMemberPrivilegesComponent;
  let fixture: ComponentFixture<OrgMemberPrivilegesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrgMemberPrivilegesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrgMemberPrivilegesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
