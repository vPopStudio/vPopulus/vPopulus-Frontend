import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';

@Component({
  selector: 'app-organisation-all-view',
  templateUrl: './organisation-all-view.component.html',
  styleUrls: ['./organisation-all-view.component.css']
})
export class OrganisationAllViewComponent implements OnInit {
  token;
  hostUrl = environment.hostUrl;
  isLoading = false;

  list = [];

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/organisations`, this.token).subscribe(r => {
      this.list = r.list;
      this.isLoading = false;
    });
  }

}
