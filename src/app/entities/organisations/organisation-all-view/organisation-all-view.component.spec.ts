import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OrganisationAllViewComponent } from './organisation-all-view.component';

describe('OrganisationAllViewComponent', () => {
  let component: OrganisationAllViewComponent;
  let fixture: ComponentFixture<OrganisationAllViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OrganisationAllViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OrganisationAllViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
