import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from '../../common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  entity;

  isLoading = true;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    // this.token = this.route.snapshot.paramMap.get("token");
    // this.token = "e2d74a4f-2d6d-4713-8fd1-cf7105f6bd55";
    // this.loadData();
  }

  loadData() {
  }

}
