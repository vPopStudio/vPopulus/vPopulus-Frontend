import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { NzMessageService } from 'ng-zorro-antd';
import { TranslateService } from '@ngx-translate/core';
import { TimeLeftPipe } from './../../../util/time-left.pipe'

@Component({
  selector: 'app-parliament-law',
  templateUrl: './parliament-law.component.html',
  styleUrls: ['./parliament-law.component.css']
})

export class ParliamentLawComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = true;

  token;
  lawId;
  law;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private translate: TranslateService,
    private timeLeft: TimeLeftPipe
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.loadData();
  }

  interval;
  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/countries/laws/${this.lawId}`, this.token).subscribe(r => {
      this.law = r;
      this.law.votesY = this.law.votes.filter(x => x.vote == "Y");
      this.law.votesN = this.law.votes.filter(x => x.vote == "N");
      this.law.votesA = this.law.votes.filter(x => x.vote == "A");
      this.law.secondsLeft = Date.parse(this.law.endDateTime) / 1000;
      this.isLoading = false;
      clearInterval(this.interval);
      this.interval = setInterval(() => {
        if (this.law.secondsLeft > 0) {
          this.law.timeLeft = this.timeLeft.transform(this.law.secondsLeft);
        }
      }, 1000)
    });
  }

  vote(option) {
    this.isLoading = true;
    this.httpService.post2(`/api/countries/laws/vote`, { lawId: this.lawId, decision: option }, this.token).subscribe(r => {
      this.translate.get(r.message).subscribe(t => {
        this.message.success(t);
        this.loadData();
      })
    }, error => {
      this.translate.get(error.error.Message).subscribe(t => {
        this.message.error(t);
        this.isLoading = false;
      })
    });
  }

}
