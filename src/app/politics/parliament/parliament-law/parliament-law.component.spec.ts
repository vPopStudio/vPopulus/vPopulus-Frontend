import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParliamentLawComponent } from './parliament-law.component';

describe('ParliamentLawComponent', () => {
  let component: ParliamentLawComponent;
  let fixture: ComponentFixture<ParliamentLawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParliamentLawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParliamentLawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
