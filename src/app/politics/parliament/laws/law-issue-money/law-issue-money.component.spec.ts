import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawIssueMoneyComponent } from './law-issue-money.component';

describe('LawIssueMoneyComponent', () => {
  let component: LawIssueMoneyComponent;
  let fixture: ComponentFixture<LawIssueMoneyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawIssueMoneyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawIssueMoneyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
