import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-minimum-wage',
  templateUrl: './law-minimum-wage.component.html',
  styleUrls: ['./law-minimum-wage.component.css']
})
export class LawMinimumWageComponent implements OnInit {

  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;

  lawId: any;
  data: any;

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    this.httpClient.get2(`/api/countries/averagesalary/${this.country.id}`, this.token).subscribe(r2 => {
      this.country.averageSalary = r2;
      this.isLoading = false;
    });
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: { minimalWage: this.data } },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
      }, error => {
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

}
