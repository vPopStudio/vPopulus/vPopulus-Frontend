import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawMinimumWageComponent } from './law-minimum-wage.component';

describe('LawMinimumWageComponent', () => {
  let component: LawMinimumWageComponent;
  let fixture: ComponentFixture<LawMinimumWageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawMinimumWageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawMinimumWageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
