import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawChangeTaxesComponent } from './law-change-taxes.component';

describe('LawChangeTaxesComponent', () => {
  let component: LawChangeTaxesComponent;
  let fixture: ComponentFixture<LawChangeTaxesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawChangeTaxesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawChangeTaxesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
