import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-change-taxes',
  templateUrl: './law-change-taxes.component.html',
  styleUrls: ['./law-change-taxes.component.css']
})
export class LawChangeTaxesComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;
  taxes: any[] = [];

  lawId: any;

  incomeTax: any;
  importTax: any;
  vat: any;

  selectedIndustryId: any;
  selectedIndustry: any;
  industries = [
    { id: 1, name: "Grain" },
    { id: 2, name: "Food" },
    { id: 3, name: "Fruit" },
    { id: 4, name: "Juice" },
    { id: 5, name: "Iron" },
    { id: 6, name: "Weapon" },
    { id: 7, name: "Oil" },
    { id: 8, name: "Moving ticket" },
    { id: 9, name: "Wood" },
    { id: 14, name: "Construction" },
    { id: 10, name: "House" },
    { id: 13, name: "Hotel" },
    { id: 12, name: "Defense system" },
    { id: 11, name: "Hospital" }
  ]

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    console.log(this.industries)
    this.httpClient.get2(`/api/countries/taxes/${this.country.id}`, this.token).subscribe(r => {
      this.taxes = r;
    });
  }

  industrySelected(value) {
    this.selectedIndustry = this.industries.find(x => x.id == value);
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`, {
      lawType: this.lawId, parameters: {
        intValue1: this.selectedIndustry.id,
        intValue2: this.incomeTax,
        intValue3: this.importTax,
        intValue4: this.vat,
      }
    }, this.token).subscribe(r => {
      var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
      win.focus();
    }, error => {
      this.translate.get(error.error.Message).subscribe(t => {
        this.message.error(t);
        this.isLoading = false;
      })
    });
  }

}
