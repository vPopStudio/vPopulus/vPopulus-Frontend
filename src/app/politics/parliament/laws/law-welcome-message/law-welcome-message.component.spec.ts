import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawWelcomeMessageComponent } from './law-welcome-message.component';

describe('LawWelcomeMessageComponent', () => {
  let component: LawWelcomeMessageComponent;
  let fixture: ComponentFixture<LawWelcomeMessageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawWelcomeMessageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawWelcomeMessageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
