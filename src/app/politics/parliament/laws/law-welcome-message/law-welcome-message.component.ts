import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-welcome-message',
  templateUrl: './law-welcome-message.component.html',
  styleUrls: ['./law-welcome-message.component.css']
})
export class LawWelcomeMessageComponent implements OnInit {
  @Input() country: any;

  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';
  lawId: any;
  data: any;

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: { WelcomeMessageContent: this.data } },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
        this.isLoading = false;
      }, error => {
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

}
