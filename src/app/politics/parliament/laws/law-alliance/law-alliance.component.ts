import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-alliance',
  templateUrl: './law-alliance.component.html',
  styleUrls: ['./law-alliance.component.css']
})
export class LawAllianceComponent implements OnInit {

  hostUrl = environment.hostUrl;
  isLoading = [false];

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  lawId: any;
  law: any; // countryLawModel
  //law: {
  //  "entityType": 0,
  //  "entityId": 0,
  //  "lawType": 1,
  //  "parameters": {
  //    "name": "string",
  //    "organisationId": 0,
  //    "regionId": 0,
  //    "countryId": 0,
  //    "welcomeMessageContent": "string",
  //    "minimalWage": 0,
  //    "minimalCitizenFee": 0,
  //    "amount": 0,
  //    "intValue1": 0,
  //    "intValue2": 0,
  //    "intValue3": 0,
  //    "intValue4": 0
  //  }
  //}


  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
  ) { }

  ngOnInit() {

    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");
    this.setEmptyLaw();
  }

  data: any;

  setEmptyLaw() {
    this.law = {
      // "entityType": this.entity.entityType,
      // "entityId": this.entity.entityId,
      "lawType": this.lawId,
      "parameters": {
        "name": "string",
        "organisationId": 0,
        "regionId": 0,
        // "countryId": this.country.id,
        "welcomeMessageContent": "string",
        "minimalWage": 0,
        "minimalCitizenFee": 0,
        "amount": 0,
        "intValue1": 0,
        "intValue2": 0,
        "intValue3": 0,
        "intValue4": 0
      }
    }
  }

  propose() {
    console.log(JSON.stringify(this.law));
    this.httpClient.post2(`/api/countries/laws`,
      this.law,
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
        })
      });
  }

}
