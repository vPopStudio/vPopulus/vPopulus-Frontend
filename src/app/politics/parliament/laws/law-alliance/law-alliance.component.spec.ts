import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawAllianceComponent } from './law-alliance.component';

describe('LawAllianceComponent', () => {
  let component: LawAllianceComponent;
  let fixture: ComponentFixture<LawAllianceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawAllianceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawAllianceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
