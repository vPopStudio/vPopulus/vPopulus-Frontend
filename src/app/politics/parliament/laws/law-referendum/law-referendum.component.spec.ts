import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawReferendumComponent } from './law-referendum.component';

describe('LawReferendumComponent', () => {
  let component: LawReferendumComponent;
  let fixture: ComponentFixture<LawReferendumComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawReferendumComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawReferendumComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
