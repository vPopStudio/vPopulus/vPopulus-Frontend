import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-donate',
  templateUrl: './law-donate.component.html',
  styleUrls: ['./law-donate.component.css']
})
export class LawDonateComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;

  finances: any[] = []; // financeModel[]
  organisations: any[] = [];

  lawId: any;

  amount: any;
  orgId: any;
  selectedOrganisation;
  reason = '';

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    var financePromise = new Promise((resolve, reject) => {
      this.httpClient.get2(`/api/finances/2/${this.country.id}`, this.token).subscribe(r => {
        resolve(r);
      });
    });

    var organisationsPromise = new Promise((resolve, reject) => {
      this.httpClient.get2(`/api/countries/${this.country.id}/nationalOrganisations`, this.token).subscribe(r => {
        resolve(r);
      });
    });

    Promise.all([financePromise, organisationsPromise]).then(results => {
      console.log(results);
      this.finances = results[0] as any[];
      this.organisations = (results[1] as any).list;
      this.isLoading = false;
    });
  }

  selectedCurrencyId: any;
  selectedCurrency: any;

  currencySelected(value) {
    this.selectedCurrency = this.finances.find(x => x.currencyId == value);
  }

  organisationSelected(value) {
    this.selectedOrganisation = this.organisations.find(x => x.id == value);
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: { organisationId: this.orgId, amount: this.amount, intValue1: this.selectedCurrency.currencyId, stringValue1: this.reason } },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
      }, error => {
        console.log(error)
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

}
