import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawDonateComponent } from './law-donate.component';

describe('LawDonateComponent', () => {
  let component: LawDonateComponent;
  let fixture: ComponentFixture<LawDonateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawDonateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawDonateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
