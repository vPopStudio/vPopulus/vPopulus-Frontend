import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-mpp',
  templateUrl: './law-mpp.component.html',
  styleUrls: ['./law-mpp.component.css']
})
export class LawMppComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country;
  countries: any[] = [];

  selectedCountryId;
  selectedCountry: any = { id: 0 };

  lawId: any;

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    this.httpClient.get2(`/api/countries/list`).subscribe(r => {
      this.countries = r;
      this.countries = this.countries.filter(x => x.id != this.country.id);
      this.isLoading = false;
    });
  }

  countrySelected(value) {
    this.selectedCountry = this.countries.find(x => x.id == value);
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`, { lawType: this.lawId, parameters: { countryId: this.selectedCountryId } }, this.token).subscribe(r => {
      var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
      win.focus();
    }, error => {
      this.translate.get(error.error.Message).subscribe(t => {
        this.message.error(t);
        this.isLoading = false;
      })
    });
  }

}
