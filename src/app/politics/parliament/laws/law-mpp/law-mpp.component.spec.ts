import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawMppComponent } from './law-mpp.component';

describe('LawMppComponent', () => {
  let component: LawMppComponent;
  let fixture: ComponentFixture<LawMppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawMppComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawMppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
