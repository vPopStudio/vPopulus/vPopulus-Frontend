import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-impeach',
  templateUrl: './law-impeach.component.html',
  styleUrls: ['./law-impeach.component.css']
})
export class LawImpeachComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;
  lawId: any;
  countryPresident;

  candidates: any[] = [];

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    this.httpClient.get2(`/api/countries/roles/${this.country.id}`, this.token).subscribe(roles => {
      this.countryPresident = roles.filter(x => x.roleId == 1)[0];
      this.isLoading = false;
    });

    this.httpClient.get2(`/api/elections/GetCountryList/1/10/true`).subscribe(r => {
      this.candidates = r;
      console.log(this.candidates);
    });
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: {} },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
        this.isLoading = false;
      }, error => {
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

}

