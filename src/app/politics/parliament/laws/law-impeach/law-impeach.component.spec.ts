import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawImpeachComponent } from './law-impeach.component';

describe('LawImpeachComponent', () => {
  let component: LawImpeachComponent;
  let fixture: ComponentFixture<LawImpeachComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawImpeachComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawImpeachComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
