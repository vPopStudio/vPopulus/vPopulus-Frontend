import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawDeclareWarComponent } from './law-declare-war.component';

describe('LawDeclareWarComponent', () => {
  let component: LawDeclareWarComponent;
  let fixture: ComponentFixture<LawDeclareWarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawDeclareWarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawDeclareWarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
