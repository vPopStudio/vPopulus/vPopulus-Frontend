import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawMoveCapitalComponent } from './law-move-capital.component';

describe('LawMoveCapitalComponent', () => {
  let component: LawMoveCapitalComponent;
  let fixture: ComponentFixture<LawMoveCapitalComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawMoveCapitalComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawMoveCapitalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
