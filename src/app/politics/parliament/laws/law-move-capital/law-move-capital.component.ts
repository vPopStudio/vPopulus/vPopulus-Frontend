import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-move-capital',
  templateUrl: './law-move-capital.component.html',
  styleUrls: ['./law-move-capital.component.css']
})
export class LawMoveCapitalComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;

  regions: any[] = []; // regionModel[]
  selectedRegionId: any;

  lawId: any;

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    this.httpClient.get2(`/api/regions/list/${this.country.id}`, this.token).subscribe(r => {
      this.regions = r;
      this.isLoading = false;
    });
  }


  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: { regionId: this.selectedRegionId } },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
      }, error => {
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }

}
