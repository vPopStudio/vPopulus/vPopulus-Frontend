import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawNatOrgComponent } from './law-nat-org.component';

describe('LawNatOrgComponent', () => {
  let component: LawNatOrgComponent;
  let fixture: ComponentFixture<LawNatOrgComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawNatOrgComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawNatOrgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
