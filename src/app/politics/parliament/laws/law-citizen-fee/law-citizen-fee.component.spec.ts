import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawCitizenFeeComponent } from './law-citizen-fee.component';

describe('LawCitizenFeeComponent', () => {
  let component: LawCitizenFeeComponent;
  let fixture: ComponentFixture<LawCitizenFeeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawCitizenFeeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawCitizenFeeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
