import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-law-propose-peace',
  templateUrl: './law-propose-peace.component.html',
  styleUrls: ['./law-propose-peace.component.css']
})
export class LawProposePeaceComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = false;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  @Input() country: any;
  countries: any[] = [];

  selectedCountryId;
  selectedCountry;

  countrySelected(value) {
    this.selectedCountry = this.countries.find(x => x.id == value);
  }

  lawId: any;

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = this.route.snapshot.paramMap.get("lawId");

    this.isLoading = true;
    this.httpClient.get2(`/api/countries/list`).subscribe(r => {
      var tmpCountries = r;
      tmpCountries = tmpCountries.filter(x => x.id != this.country.id);
      this.httpClient.get2(`/api/military/wars`, this.token).subscribe(wars => {
        wars.list.forEach(war => {
          if (war.country1.entityId == this.country.id || war.country2.entityId == this.country.id) {
            this.countries.push(tmpCountries.filter(x => x.id == (war.country1.entityId == this.country.id ? war.country2.entityId : war.country1.entityId))[0])
          }
          this.countries = this.countries.filter(x => x != undefined);
        });
        this.isLoading = false;
      });
    });
  }

  propose() {
    this.isLoading = true;
    this.httpClient.post2(`/api/countries/laws`,
      { lawType: this.lawId, parameters: { countryId: this.selectedCountryId } },
      this.token).subscribe(r => {
        var win = window.open(`${this.hostUrl}/parliament/law/${r}`, '_blank');
        win.focus();
      }, error => {
        this.translate.get(error.error.Message).subscribe(t => {
          this.message.error(t);
          this.isLoading = false;
        })
      });
  }
}
