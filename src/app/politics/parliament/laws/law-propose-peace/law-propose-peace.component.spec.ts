import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawProposePeaceComponent } from './law-propose-peace.component';

describe('LawProposePeaceComponent', () => {
  let component: LawProposePeaceComponent;
  let fixture: ComponentFixture<LawProposePeaceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawProposePeaceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawProposePeaceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
