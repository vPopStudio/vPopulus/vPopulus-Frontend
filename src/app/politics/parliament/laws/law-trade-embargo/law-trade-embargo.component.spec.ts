import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LawTradeEmbargoComponent } from './law-trade-embargo.component';

describe('LawTradeEmbargoComponent', () => {
  let component: LawTradeEmbargoComponent;
  let fixture: ComponentFixture<LawTradeEmbargoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LawTradeEmbargoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LawTradeEmbargoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
