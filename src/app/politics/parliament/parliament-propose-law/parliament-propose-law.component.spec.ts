import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParliamentProposeLawComponent } from './parliament-propose-law.component';

describe('ParliamentProposeLawComponent', () => {
  let component: ParliamentProposeLawComponent;
  let fixture: ComponentFixture<ParliamentProposeLawComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParliamentProposeLawComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParliamentProposeLawComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
