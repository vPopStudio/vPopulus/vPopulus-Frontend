import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-parliament-propose-law',
  templateUrl: './parliament-propose-law.component.html',
  styleUrls: ['./parliament-propose-law.component.css']
})
export class ParliamentProposeLawComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = [false, false];

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  lawId: any;

  countries: any[] = [];

  countryId: any;
  country: any;

  entity: any;

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.lawId = +this.route.snapshot.paramMap.get("lawId");

    this.isLoading[0] = true;
    this.httpClient.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(r => {
      this.entity = r.entity;

      this.httpClient.get2(`/api/countries/${this.entity.countryId}`, this.token).subscribe(r => {
        this.country = r;
        this.isLoading[0] = false;
      });
    });
  }

}
