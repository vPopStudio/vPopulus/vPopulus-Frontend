import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-candidate-page',
  templateUrl: './candidate-page.component.html',
  styleUrls: ['./candidate-page.component.css']
})
export class CandidatePageComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token: string;
  isLoading = [false, false];

  countries: any[] = [];
  candidatesOfficial: any[] = [];
  candidatesUnofficial: any[] = [];

  elections = [
    { name: "Country Presidential", id: 1, icon: 'president_elect' },
    { name: "Congress", id: 2, icon: 'congress_elect' },
    { name: "Presidential", id: 3, icon: 'party_elect' }
  ];

  selectedElection = 0;
  selectedCountryId;

  entityType;
  entityId;
  partyId;
  electionType = 0;
  countryId = 0;
  canCandidate;
  isOfficial = false;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private message: NzMessageService,) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadEntityType();
  }

  loadEntityType() {
    this.httpService.get2(`/api/auth/GetData/${this.token}`,).subscribe(r => {
      this.entityType = r.entityType;
      this.entityId = r.entityId;
      this.loadData();
      if(r.entityType == 1) {
        this.httpService.get2(`/api/citizens/GetCurrent`, this.token).subscribe(rr => {
          this.partyId = rr.partyId;
        });
      }
      if(r.entityType == 4) {
        this.httpService.get2(`/api/party/GetSingle/${r.entityId}`).subscribe(rr => {

        });
      }
    });
  }

  loadData() {
    this.httpService.get2(`/api/countries/list`).subscribe(r => {
      this.countries = r;
    });
  }

  loadCandidates() {
    this.httpService.get2(`/api/elections/GetCountryList/${this.electionType}/${this.countryId}/${this.isOfficial}`).subscribe(r => {
      this.countries = r;
    });
  }

  electionSelected(value) {
    this.electionType = value;
    this.selectedCountryId = 0;
  }

  countrySelected(value) {
    this.countryId = value;
    if(this.entityType == 1)
      this.httpService.get2(`/api/elections/GetById/${this.electionType}/${this.entityId}`, this.token).subscribe(rrr => {
        this.canCandidate = rrr == null ? true : false;
      });
    if(this.electionType == 3)
      this.httpService.get2(`/api/elections/GetPartyList/${this.electionType}/${this.partyId}`).subscribe(r => {
        this.candidatesUnofficial = r;
      });
    else
      this.httpService.get2(`/api/elections/GetCountryList/${this.electionType}/${value}/true`).subscribe(r => {
        this.candidatesOfficial = r;
      });
    if(this.electionType != 3)
    this.httpService.get2(`/api/elections/GetCountryList/${this.electionType}/${value}/false`).subscribe(r => {
      this.candidatesUnofficial = r;
    });
  }

  makeOfficial(isOfficial, candidateId) {
    this.httpService.get2(`/api/elections/MakeOfficial/${this.electionType}/${candidateId}/${this.entityId}/${isOfficial}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.isLoading[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }

  withdraw(value) {
    this.httpService.get2(`/api/elections/Withdraw/${this.electionType}/${this.entityId}`, this.token).subscribe(r => {
      for(var i = 0; i < this.candidatesUnofficial.length; i++) {
        if(this.candidatesUnofficial[i].citizenId == value)
          this.candidatesUnofficial.splice(i,1);
      }
      this.canCandidate = true;
      this.message.success(r.message);
      this.isLoading[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }

  candidate() {
    this.httpService.get2(`/api/elections/Candidate/${this.electionType}/${this.entityId}`, this.token).subscribe(r => {
      this.canCandidate = false;
      this.message.success(r.message);
      this.isLoading[3] = false;
      if(this.electionType != 3)
        this.httpService.get2(`/api/elections/GetCountryList/${this.electionType}/${this.countryId}/false`).subscribe(r => {
        this.candidatesUnofficial = r;
      });
      else
        this.httpService.get2(`/api/elections/GetPartyList/${this.electionType}/${this.partyId}`).subscribe(r => {
        this.candidatesUnofficial = r;
      });
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }
}
