import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-vote-page',
  templateUrl: './vote-page.component.html',
  styleUrls: ['./vote-page.component.css']
})
export class VotePageComponent implements OnInit {

  hostUrl = environment.hostUrl;
  token: string;
  countries: any[] = [];
  regions: any[] = [];
  candidates: any[] = [];
  isLoading = [false, false];

  parties = [
    {Name: "PartyTest", Id: 1}
  ];

  electionType;
  elections = ["System","Country Presidential", "Congress", "Presidential"];

  CountryName = "Italy";

  RegionName = "Veneto";

  canVote = true;
  nowCanVote = false;

  citizenId;

  
  selectedCountryId;
  selectedRegionId;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private message: NzMessageService) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadData();
  }

  getName(id) {
    return "test";
  }

  loadData() {
    this.httpService.get2(`/api/countries/GetAllWithRegions`).subscribe(r => {
      this.countries = r;
    });
    this.httpService.get2(`/api/citizens/GetCurrent`, this.token).subscribe(r => {
      this.citizenId = r.id;
      this.httpService.get2(`/api/elections/HasVoted/${this.electionType}/${r.id}`).subscribe(r => {
        this.canVote = r;
        //debug//
        this.canVote = true;
        console.log("vote", r);
      });
    });
  }

  loadRegions() {
    if(this.electionType == 1) {
      this.loadCandidates();
    }
    if(this.electionType == 2) {
      this.httpService.get2(`/api/regions/list/${this.selectedCountryId}`).subscribe(r => {
        this.regions = r;
      });
    }
    if(this.electionType == 3) {
      this.loadCandidates();
    }
  }

  loadCandidates() {
    this.nowCanVote = true;
    this.httpService.get2(`/api/elections/GetElectionId/${this.electionType}`).subscribe(r => {
      this.httpService.get2(`/api/elections/GetFinalList/${r}/${true}`).subscribe(r => {
        console.log("erre", r);
        this.candidates = r;
      });
    });
  }

  vote(candidateId) {
    console.log(candidateId);
    this.httpService.get2(`/api/elections/Vote/${this.electionType}/${this.citizenId}/${candidateId}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.isLoading[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }

}
