import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ElectionsCandidateViewComponent } from './elections-candidate-view.component';

describe('ElectionsCandidateViewComponent', () => {
  let component: ElectionsCandidateViewComponent;
  let fixture: ComponentFixture<ElectionsCandidateViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ElectionsCandidateViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ElectionsCandidateViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
