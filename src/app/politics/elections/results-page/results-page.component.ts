import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-elections-results-page',
  templateUrl: './results-page.component.html',
  styleUrls: ['./results-page.component.css']
})
export class ResultsPageComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = [false, false];

  selectedElection = 0;
  selectedDate = 0;

  elections = [
    { name: "Country Presidential", id: 1, icon: 'president_elect' },
    { name: "Congress", id: 2, icon: 'congress_elect' },
    { name: "Presidential", id: 3, icon: 'party_elect' }
  ];

  countries: any[] = [];
  selectedCountryId;
  selectedCountry: any = { id: 0 };

  electionDates: any[] = [];
  candidates: any[] = [];

  DateSelected = false;

  ElectionType = "PartyPresident";

  constructor(
    private httpService: GenericHttpService,
  ) { }

  ngOnInit() {
    this.loadList();
  }

  loadList() {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/countries/list`).subscribe(r => {
      this.countries = r;
      this.isLoading[0] = false;
    });
  }

  electionSelected(value) {
    this.httpService.get2(`/api/elections/GetDates/${this.selectedElection}`).subscribe(r => {
      this.electionDates = r;
    });

    this.DateSelected = false;
    this.selectedCountryId = 0;
  }

  countrySelected(value) {
    this.selectedCountry = this.countries.filter(x => x.id == this.selectedCountryId)[0];
    this.selectedDate = undefined;
  }

  dateSelected(value) {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/elections/results/${this.selectedElection}/${this.selectedCountryId}/${this.selectedDate}`).subscribe(r => {
      this.candidates = r;
      this.DateSelected = true;
      this.isLoading[0] = false;
    });
  }

}
