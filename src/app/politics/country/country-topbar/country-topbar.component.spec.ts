import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryTopbarComponent } from './country-topbar.component';

describe('CountryTopbarComponent', () => {
  let component: CountryTopbarComponent;
  let fixture: ComponentFixture<CountryTopbarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryTopbarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryTopbarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
