import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryPoliticsViewComponent } from './country-politics-view.component';

describe('CountryPoliticsViewComponent', () => {
  let component: CountryPoliticsViewComponent;
  let fixture: ComponentFixture<CountryPoliticsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryPoliticsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryPoliticsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
