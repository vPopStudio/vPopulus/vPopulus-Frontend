import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatAlliesComponent } from './stat-allies.component';

describe('StatAlliesComponent', () => {
  let component: StatAlliesComponent;
  let fixture: ComponentFixture<StatAlliesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatAlliesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatAlliesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
