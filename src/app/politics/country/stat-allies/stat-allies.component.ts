import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stat-allies',
  templateUrl: './stat-allies.component.html',
  styleUrls: ['./stat-allies.component.css']
})
export class StatAlliesComponent implements OnInit {
  hostUrl = environment.hostUrl;

  @Input() country: any; // cacheEntity
  @Input() activeLaws: any[] = []; 

  constructor(
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }
}
