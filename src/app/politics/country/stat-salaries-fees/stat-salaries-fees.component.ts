import { Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stat-salaries-fees',
  templateUrl: './stat-salaries-fees.component.html',
  styleUrls: ['./stat-salaries-fees.component.css']
})
export class StatSalariesFeesComponent implements OnInit {
  @Input() country: any; // with min salary, avg salary, citizen fee and currency name

  constructor(
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }


}
