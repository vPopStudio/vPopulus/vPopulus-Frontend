import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatSalariesFeesComponent } from './stat-salaries-fees.component';

describe('StatSalariesFeesComponent', () => {
  let component: StatSalariesFeesComponent;
  let fixture: ComponentFixture<StatSalariesFeesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatSalariesFeesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatSalariesFeesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
