import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-citizenship-list',
  templateUrl: './citizenship-list.component.html',
  styleUrls: ['./citizenship-list.component.css']
})
export class CitizenshipListComponent implements OnInit {
  hostUrl = environment.hostUrl;
  countryId = 38; // We will pass countryId from game

  token: string;
  applyReason: string;

  citizenshipsRequests: any[] = [];
  requestedPage = 1;
  requestedPageSize = 20;
  requestedTotalEntries = 0;
  canDecide = false;
  canRequest = false;

  decidedList: any[] = [];
  decidedPage = 1;
  decidedPageSize = 20;
  decidedTotalEntries = 0;

  isLoading: any[] = [false, false];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService
  ) { }



  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.countryId = +this.route.snapshot.paramMap.get("countryId");
    this.loadDecidedList();
    this.loadRequestList();
  }

  loadDecidedList() {  // This is for requests with deciions
    this.isLoading[0] = true;
    this.httpService.get2(`/api/country/citizenship/GetDecidedList/${this.countryId}/${this.decidedPage}/${this.decidedPageSize}`).subscribe(r => {
      this.decidedList = r.list;
      this.decidedTotalEntries = r.totalEntries;
      this.isLoading[0] = false;
    });
  }

  loadRequestList() {    // Requests pending
    this.isLoading[0] = true;
    this.httpService.get2(`/api/country/citizenship/GetRequestList/${this.countryId}/${this.requestedPage}/${this.requestedPageSize}`, this.token).subscribe(r => {
      this.citizenshipsRequests = r.list;
      this.requestedTotalEntries = r.totalEntries;
      this.canDecide = r.canDecide;
      this.canRequest = r.canRequest;
      this.isLoading[0] = false;
    });
  }

  apply() {
    if (this.applyReason === "") {
      this.message.error("Please provide reason!")
      return;
    }
    this.isLoading[1] = true;
    this.httpService.post2(`/api/country/citizenship/RequestPost`, { reason: this.applyReason }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.isLoading[1] = false;
      this.loadRequestList();
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
      this.loadRequestList();
    });
  }

  manage(requestId, decision) {
    this.isLoading[0] = true;
    this.httpService.post2(`/api/country/citizenship/Manage`, { requestId: requestId, decision: decision }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadRequestList();
      this.loadDecidedList();
    }, error => {
      console.log(error)
      this.message.error(error.error.message);
      this.loadRequestList();
      this.loadDecidedList();
    });
  }

}
