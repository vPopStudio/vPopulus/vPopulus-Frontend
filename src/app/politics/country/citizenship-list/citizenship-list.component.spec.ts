import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CitizenshipListComponent } from './citizenship-list.component';

describe('CitizenshipListComponent', () => {
  let component: CitizenshipListComponent;
  let fixture: ComponentFixture<CitizenshipListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CitizenshipListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CitizenshipListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
