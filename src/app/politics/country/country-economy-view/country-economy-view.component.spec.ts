import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryEconomyViewComponent } from './country-economy-view.component';

describe('CountryEconomyViewComponent', () => {
  let component: CountryEconomyViewComponent;
  let fixture: ComponentFixture<CountryEconomyViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryEconomyViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryEconomyViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
