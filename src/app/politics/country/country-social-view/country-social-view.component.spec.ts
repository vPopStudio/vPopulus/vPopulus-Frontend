import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountrySocialViewComponent } from './country-social-view.component';

describe('CountrySocialViewComponent', () => {
  let component: CountrySocialViewComponent;
  let fixture: ComponentFixture<CountrySocialViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountrySocialViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountrySocialViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
