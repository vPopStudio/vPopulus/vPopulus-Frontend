import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { Route, ActivatedRoute } from '@angular/router';
import { environment } from 'src/environments/environment';
import { ConsoleLogger } from 'src/assets/lib/@aspnet/signalr/src/Utils';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-country-administration-view',
  templateUrl: './country-administration-view.component.html',
  styleUrls: ['./country-administration-view.component.css']
})
export class CountryAdministrationViewComponent implements OnInit {
  token;
  countryId;
  isLoading = false;
  hostUrl = environment.hostUrl;

  list = [];
  totalEntries = 0;
  canPropose = false;

  page = 1;
  pageSize = 25;

  selectedProposeLawId = 0;
  lawsAvailableToPropose = [];


  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private messageService: NzMessageService,
  ) { }

  ngOnInit() {
    this.countryId = this.route.snapshot.paramMap.get("countryId");
    this.token = this.route.snapshot.paramMap.get("token");

    this.load();
  }

  load() {
    this.isLoading = true;
    this.httpService.get2(`/api/countries/laws/list/${this.countryId}?page=${this.page}&`).subscribe(r => {
      this.list = r.list;
      this.totalEntries = r.totalEntries;
      this.canPropose = true;
      if (this.canPropose === true) {
        this.httpService.get2(`/api/countries/laws/availableToPropose`).subscribe(a => {
          this.lawsAvailableToPropose = a;
        });
      }
      this.isLoading = false;
    });
  }

  lawSelected(lawId) {
    window.open(this.hostUrl + `/parliament/propose/${this.selectedProposeLawId}`, '_blank');
  }

  resign() {
    this.isLoading = true;
    this.httpService.post2(`/api/countries/roles/resign`, this.token).subscribe(r => {
      this.messageService.success(`You have resigned from government position.`);
      this.isLoading = false;
    });
  }

}
