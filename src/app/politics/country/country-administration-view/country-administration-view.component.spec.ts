import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryAdministrationViewComponent } from './country-administration-view.component';

describe('CountryAdministrationViewComponent', () => {
  let component: CountryAdministrationViewComponent;
  let fixture: ComponentFixture<CountryAdministrationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryAdministrationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryAdministrationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
