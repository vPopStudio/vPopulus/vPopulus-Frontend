import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CountryMilitaryViewComponent } from './country-military-view.component';

describe('CountryMilitaryViewComponent', () => {
  let component: CountryMilitaryViewComponent;
  let fixture: ComponentFixture<CountryMilitaryViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CountryMilitaryViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CountryMilitaryViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
