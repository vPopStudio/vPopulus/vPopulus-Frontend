import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StatEmbargosComponent } from './stat-embargos.component';

describe('StatEmbargosComponent', () => {
  let component: StatEmbargosComponent;
  let fixture: ComponentFixture<StatEmbargosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StatEmbargosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StatEmbargosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
