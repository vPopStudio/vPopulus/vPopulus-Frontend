import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-stat-embargos',
  templateUrl: './stat-embargos.component.html',
  styleUrls: ['./stat-embargos.component.css']
})
export class StatEmbargosComponent implements OnInit {

  hostUrl = environment.hostUrl;

  @Input() country: any; // cacheEntity
  @Input() activeLaws: any[] = []; // countryLawActiveModel[]

  constructor(
    private translate: TranslateService,
  ) { }

  ngOnInit() {
  }

}
