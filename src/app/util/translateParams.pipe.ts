import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translateParams'
})
export class TranslateParamsPipe implements PipeTransform {

  transform(text: string, args: string): any {
    var argsJson = JSON.parse(args);
    Object.keys(argsJson).forEach(index => {
      text = text.replace('{{' + index + '}}', argsJson[index][index])
    });
    return text;
  }

}
