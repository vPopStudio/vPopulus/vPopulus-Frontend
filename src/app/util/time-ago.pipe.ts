import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeAgo'
})
export class TimeAgoPipe implements PipeTransform {

  transform(pDate: string, args?: any): any {
    var date = new Date(Date.parse(pDate));
    var dateNow = new Date(Date.parse(new Date().toUTCString()))
    dateNow.setHours(dateNow.getHours() - 1);

    var diff = (dateNow.getTime() - date.getTime());

    var days = Math.floor(diff / (1000 * 60 * 60 * 24));
    diff -= days * (1000 * 60 * 60 * 24);

    var years = 0;
    if (days > 365)
      years = days / 365;

    if (years > 0) {
      if (years > 1)
        return `${years} years`;
      else
        return `${years} year`;
    }
    var months = 0;
    if (days > 31)
      months = days / 31;

    if (months > 0) {
      if (months > 1)
        return `${months} months`;
      else
        return `${months} month`;
    }

    if (days > 0) {
      if (days > 1)
        return `${days} days`;
      else
        return `${days} day`;
    }

    var hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);

    if (hours > 0) {
      if (hours > 1)
        return `${hours} hours`;
      else
        return `${hours} hour`;
    }

    var mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);

    if (mins > 0) {
      if (mins > 1)
        return `${mins} minutes`;
      else
        return `${mins} minute`;
    }

    var seconds = Math.floor(diff / (1000));
    diff -= seconds * (1000);

    if (seconds > 0) {
      if (seconds > 1)
        return `${seconds} seconds`;
      else
        return `${seconds} second`;
    }

    return `0 seconds`;
  }

}
