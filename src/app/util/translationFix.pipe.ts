import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'translateParamsFix'
})
export class TranslationFixPipe implements PipeTransform {

  transform(key: string, args?: any): any {
    var paramCount = 0;
    while (true) {
      if (key.includes('{' + paramCount + '}')) {
        key = key.replace('{' + paramCount + '}', '{{' + paramCount + '}}')
        paramCount += 1;
      } else
        break;
    }
    return key;
  }

}
