import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'timeLeft'
})
export class TimeLeftPipe implements PipeTransform {

  transform(seconds: number, args?: any): any {
    var dateNow = new Date(Date.parse(new Date().toUTCString()))
    dateNow.setHours(dateNow.getHours() - 1);

    var timestamp = seconds * 1000;
    if(timestamp < dateNow.getTime())
      return "00:00:00";

    var diff = (timestamp - dateNow.getTime());

    var days = Math.floor(diff / (1000 * 60 * 60 * 24));
    diff -=  days * (1000 * 60 * 60 * 24);

    var hours = Math.floor(diff / (1000 * 60 * 60));
    diff -= hours * (1000 * 60 * 60);

    var mins = Math.floor(diff / (1000 * 60));
    diff -= mins * (1000 * 60);

    var seconds = Math.floor(diff / (1000));
    diff -= seconds * (1000);

    return `${hours.toString().padStart(2, '0')}:${mins.toString().padStart(2, '0')}:${seconds.toString().padStart(2, '0')}`;
  }

}
