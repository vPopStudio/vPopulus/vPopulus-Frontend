import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';

@Component({
  selector: 'app-company-all-view',
  templateUrl: './company-all-view.component.html',
  styleUrls: ['./company-all-view.component.css']
})
export class CompanyAllViewComponent implements OnInit {
  token;
  hostUrl = environment.hostUrl;
  isLoading = false;

  list = [];

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/companies`, this.token).subscribe(r => {
      this.list = r.list;
      console.log(this.list)
      this.isLoading = false;
    });
  }

}
