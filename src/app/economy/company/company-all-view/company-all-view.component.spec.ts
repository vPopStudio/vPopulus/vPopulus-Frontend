import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CompanyAllViewComponent } from './company-all-view.component';

describe('CompanyAllViewComponent', () => {
  let component: CompanyAllViewComponent;
  let fixture: ComponentFixture<CompanyAllViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CompanyAllViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CompanyAllViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
