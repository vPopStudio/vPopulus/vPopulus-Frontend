import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingViewInventoryComponent } from './trading-view-inventory.component';

describe('TradingViewInventoryComponent', () => {
  let component: TradingViewInventoryComponent;
  let fixture: ComponentFixture<TradingViewInventoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingViewInventoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingViewInventoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
