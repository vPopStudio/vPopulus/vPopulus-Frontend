import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { FormsModule } from '@angular/forms'; // <-- NgModel lives here
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-trading-view-inventory',
  templateUrl: './trading-view-inventory.component.html',
  styleUrls: ['./trading-view-inventory.component.css']
})
export class TradingViewInventoryComponent implements OnInit {
  hostUrl = environment.hostUrl;

  @Input() inventory: any[] = [];
  @Input() tradeId: number;
  @Input() visible: boolean;
  @Output() itemAddedEvent = new EventEmitter<any>();

  isLoading: any[] = [false];

  selectedItem: any;
  selectedAmount: number = 0;

  selectItem(item) {
    this.selectedItem = item;
    this.selectedAmount;
  }

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");

    this.getInventory();
  }

  getInventory() {
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/inventory`, this.token).subscribe(r => {
      this.inventory = r;
      this.isLoading[0] = false;
    });
  }

  addItem() {
    console.log(JSON.stringify({
      tradeId: this.tradeId,
      isCurrency: false,
      itemId: this.selectedItem.itemId,
      quality: this.selectedItem.quality,
      amount: this.selectedAmount
    }));
    this.httpClient.post2(`/api/trades/items/AddItem`,
      {
        tradeId: this.tradeId,
        isCurrency: false,
        itemId: this.selectedItem.itemId,
        quality: this.selectedItem.quality,
        amount: this.selectedAmount
      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
          this.selectedAmount = 0;
          this.notifyParent();
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
          this.notifyParent();
        })
      });
  }

  notifyParent() {
    this.itemAddedEvent.emit(true);
  }

}
