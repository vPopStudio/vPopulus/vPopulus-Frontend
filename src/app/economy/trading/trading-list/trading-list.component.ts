import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { debug } from 'util';


@Component({
  selector: 'app-trading-list',
  templateUrl: './trading-list.component.html',
  styleUrls: ['./trading-list.component.css']
})
export class TradingListComponent implements OnInit {
  hostUrl = environment.hostUrl;

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  trades: any[] = [];
  //currentEntity: any;
  entityType: number = 0;
  entityId: number = 0;

  isLoading: any[] = [true];

  page: number = 1;
  pageSize: number = 10;
  //totalEntries = 0;

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/trades/${this.page}/${this.pageSize}`, this.token).subscribe(r => {
      this.trades = r.list.reverse(); // reverse to show newest first
    });
    this.getEntity();
    this.isLoading[0] = false;
  }

  getEntity() {
    this.httpClient.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(r => {
      this.entityId = r.entityId;
      this.entityType = r.entityType;
    });

  }

}
