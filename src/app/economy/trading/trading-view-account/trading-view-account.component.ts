import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-trading-view-account',
  templateUrl: './trading-view-account.component.html',
  styleUrls: ['./trading-view-account.component.css']
})
export class TradingViewAccountComponent implements OnInit {
  hostUrl = environment.hostUrl;

  @Input() account: any[] = [];
  @Input() tradeId: number;
  @Input() visible: boolean;
  @Output() itemAddedEvent = new EventEmitter<any>();

  isLoading: any[] = [false];

  selectedCurrency: any;
  selectedAmount: number;

  selectCurrency(cc) {
    this.selectedCurrency = cc;
    this.selectedAmount = 0;
  }

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  constructor(
    private httpClient: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");

    this.getFinance();
  }

  getFinance() {
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/finances`, this.token).subscribe(r => {
      this.account = r;
      this.isLoading[0] = false;
    });
  }

  addItem() {
    console.log(JSON.stringify({
      tradeId: this.tradeId,
      isCurrency: true,
      itemId: this.selectedCurrency.currencyId,
      quality: 0,
      amount: this.selectedAmount
    }));
    this.httpClient.post2(`/api/trades/items/AddItem`,
      {
        tradeId: this.tradeId,
        isCurrency: true,
        itemId: this.selectedCurrency.currencyId,
        quality: 0,
        amount: this.selectedAmount
      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);
          this.selectedAmount = 0;
          this.notifyParent();
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);
          this.notifyParent();
        })
      });
  }

  notifyParent() {
    this.itemAddedEvent.emit(true);
  }

}
