import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TradingViewAccountComponent } from './trading-view-account.component';

describe('TradingViewAccountComponent', () => {
  let component: TradingViewAccountComponent;
  let fixture: ComponentFixture<TradingViewAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TradingViewAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TradingViewAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
