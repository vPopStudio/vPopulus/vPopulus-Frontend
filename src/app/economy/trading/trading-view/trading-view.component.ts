import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { NzMessageService } from 'ng-zorro-antd';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { debug } from 'util';
import { QualityStarsPipe } from 'src/app/util/quality-stars.pipe';
import { FilterByEntityPipe } from 'src/app/util/filter.pipe';


@Component({
  selector: 'app-trading-view',
  templateUrl: './trading-view.component.html',
  styleUrls: ['./trading-view.component.css']
})
export class TradingViewComponent implements OnInit {
  hostUrl = environment.hostUrl;
  isLoading = [false];

  // TOKEN - TEMPORARY - LATER USE ONE AFTER LOGIN
  token = '';

  tradeId: number;

  trade: any = { isFinished: false };

  items: any[] = []
  selectedItemId: any;

  currencies: any[] = []

  selectedCurrency: any;

  selectCurrency(item) {
    this.selectCurrency = item;
  }

  constructor(
    private httpClient: GenericHttpService,
    private message: NzMessageService,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.tradeId = +this.route.snapshot.paramMap.get("tradeId"); // + converts string to number

    this.getEntity();
    this.getTrade(this.tradeId);
  }

  isDonate: boolean;
  isTrade: boolean;

  getTradeType() {
    if (this.isDonate) {
      return 'donate';
    }
    else {
      return 'trade';
    }
  }

  childCurrentValue: string;

  itemAddedEventHandler(selected) {
    if (selected) {
      this.childCurrentValue = "Value received from child: " + selected;
      this.getTrade(this.tradeId); // refresh trade, item was added
    }
  }

  entityType: number = 0;
  entityId: number = 0;

  getEntity() {
    this.httpClient.get2(`/api/auth/GetData/${this.token}`, this.token).subscribe(r => {
      this.entityId = r.entityId;
      this.entityType = r.entityType;
    });
  }


  removeItem(item) {
    if (item.item) {
      this.selectedItemId = item.item.itemId;
    }
    else if (item.currency) {
      this.selectedItemId = item.currency.currencyId;
    }

    console.log(JSON.stringify({
      tradeId: this.tradeId,
      itemId: this.selectedItemId,
    }));
    this.httpClient.post2(`/api/trades/items/RemoveItem`,
      {
        tradeId: this.tradeId,
        itemId: item
      },
      this.token).subscribe(r => {
        this.translate.get(r.message).subscribe(t => {
          this.message.success(t);

          this.getTrade(this.tradeId);
        })
      }, error => {
        this.translate.get(error.error.message).subscribe(t => {
          this.message.error(t);

          this.getTrade(this.tradeId);
        })
      });
  }

  getTrade(tradeId) {
    this.isLoading[0] = true;
    this.httpClient.get2(`/api/trades/${tradeId}`, this.token).subscribe(r => {
      this.trade = r;
      this.isDonate = (r.isInstant === true);
      this.isTrade = (r.isInstant === false);
      this.isLoading[0] = false;
    }, error => {
      this.router.navigateByUrl(`/trades/${this.token}`);
    });
  }

  closeTrade() {
    this.isLoading[0] = true;
    this.httpClient.post2(`/api/trades/Close`, { tradeId: this.tradeId }, this.token).subscribe(r => {
      this.translate.get(r.message).subscribe(t => {
        this.message.success(t);
        this.getTrade(this.tradeId);
      })
    }, error => {
      this.translate.get(error.error.message).subscribe(t => {
        this.message.error(t);
      })
    });
  }

  showAcceptTradeButton() {
    this.trade.isFinished == false && this.trade.items.length > 0;
  }

  approveTrade() {
    this.isLoading[0] = true;
    this.httpClient.post2(`/api/trades/Accept`, { tradeId: this.tradeId }, this.token).subscribe(r => {
      this.translate.get(r.message).subscribe(t => {
        this.message.success(t);
        this.getTrade(this.tradeId);
      })
    }, error => {
      this.translate.get(error.error.message).subscribe(t => {
        this.message.error(t);
        this.getTrade(this.tradeId);
      })
    });
  }
}
