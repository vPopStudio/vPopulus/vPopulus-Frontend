import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { promise } from 'protractor';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-notifications-list',
  templateUrl: './notifications-list.component.html',
  styleUrls: ['./notifications-list.component.css']
})
export class NotificationsListComponent implements OnInit {

  hostUrl = environment.hostUrl;

  allChecked = false;

  token: string;
  notificationCount = 1;
  categoryId = 1;
  notificationPage = 1;
  notificationPageSize = 25;
  categories = [];

  notificationsAll: any[] = [];

  notifications: any[] = [];

  isLoading: any[] = [true];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadList();
  }

  loadList() {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/notifications/${this.notificationPage}/${this.notificationPageSize}`, this.token).subscribe(r => {
      this.notifications = r.list;
      this.notificationCount = r.totalEntries;
      this.isLoading[0] = false;
      this.allChecked = false;
    });
  }

  checkAll() {
    for (var item of this.notifications) {
      item.isChecked = !item.isChecked;
    };
  }

  remove() {
    this.httpService.delete(`/api/notifications/${this.notifications.filter(x => x.isChecked === true).map(x => x.id).join(',')}`, this.token).subscribe(r => {
      this.loadList();
      this.message.success(r.message);
    });
  }

}
