import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from '../services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ranking',
  templateUrl: './ranking.component.html',
  styleUrls: ['./ranking.component.css']
})
export class RankingComponent implements OnInit {
  token = undefined;
  hostUrl = environment.hostUrl;
  isLoading = false;

  types = [
    { id: 1, entityType: 1, icon: `${this.hostUrl}/assets/img/ico/nav/citizens-bl.png`, name: 'Citizens: Experience', subType: 0 },
    { id: 2, entityType: 1, icon: `${this.hostUrl}/assets/img/ico/nav/citizens-bl.png`, name: 'Citizens: Military rank', subType: 1 },
    { id: 3, entityType: 1, icon: `${this.hostUrl}/assets/img/ico/nav/citizens-bl.png`, name: 'Citizens: Medals', subType: 2 },
    { id: 4, entityType: 5, icon: `${this.hostUrl}/assets/img/ico/nav/countries-bl.png`, name: 'Countries: Population', subType: 0 },
    { id: 5, entityType: 4, icon: `${this.hostUrl}/assets/img/ico/nav/parties-bl.png`, name: 'Parties: Member count', subType: 0 },
    { id: 6, entityType: 3, icon: `${this.hostUrl}/assets/img/ico/nav/armies-bl.png`, name: 'Armies: Member count', subType: 0 },
    { id: 7, entityType: 3, icon: `${this.hostUrl}/assets/img/ico/nav/armies-bl.png`, name: 'Armies: Member damage', subType: 1 },
    { id: 8, entityType: 8, icon: `${this.hostUrl}/assets/img/ico/nav/newspaper-bl.png`, name: 'Newspapers: Subscriber count', subType: 0 },
  ];
  selectedTypeId;
  selectedType;

  countries = [];
  selectedCountryId;
  selectedCountry;

  page = 1;
  pageSize = 20;

  list = [];
  totalEntries = 0;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.httpService.get2(`/api/countries/list`).subscribe(r => {
      this.countries = r;
      if (this.token != undefined) {
        this.httpService.get2(`/api/auth/getdata/${this.token}`).subscribe(user => {
          this.selectedCountryId = user.entity.countryId;
          this.selectCountry(0);
          this.selectedTypeId = 1;
          this.selectType(1);
        });
      }
    });
  }

  loadData() {
    if (this.selectedTypeId == 4) this.selectedCountryId = 0;
    if (this.selectedTypeId != 4 && this.selectedCountryId == 0) this.selectedCountryId = undefined;
    if (this.selectedCountryId == undefined || this.selectedTypeId == undefined) { this.list = []; return; }

    this.isLoading = true;
    this.httpService.get2(`/api/ranking/${this.selectedCountryId}/${this.selectedType.entityType}/${this.selectedType.subType}?page=${this.page}&pageSize=${this.pageSize}`).subscribe(r => {
      this.list = r.list;
      console.log(this.list);
      this.totalEntries = r.totalEntries;
      this.isLoading = false;
    });
  }

  selectCountry(value) {
    this.selectedCountry = this.countries.filter(x => x.id == this.selectedCountryId)[0];
    this.loadData();
  }

  selectType(value) {
    this.selectedType = this.types.filter(x => x.id == this.selectedTypeId)[0];
    this.loadData();
  }

}
