import { Component, OnInit } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { ActivatedRoute } from '@angular/router';
import { promise } from 'protractor';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-common-inventory',
  templateUrl: './inventory.component.html',
  styleUrls: ['./inventory.component.css']
})
export class InventoryComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token: string;
  items: any[] = [];
  citizen: any[] = [];
  citizenStats: any = {};
  region: any[] = [];
  country: any[] = [];

  isLoading: any[] = [true];
  EntityType = 9;
  HospitalQ = 0;
  WeaponPriority = 1;
  ItemAmount = 0;
  citizenId = 0;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService
    ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadList();
    this.loadEntityType();
  }

  loadEntityType() {
    this.httpService.get2(`/api/auth/GetData/${this.token}`,).subscribe(r => {
      console.log("entitype", r);
      this.EntityType = r.entityType;
      this.loadData();
    });
  }

  loadList() {
    this.isLoading[0] = true;
    this.httpService.get2(`/api/inventory`, this.token).subscribe(r => {
      this.items = r;
      r.forEach(item => {
        console.log("amount", item.amount);
        this.ItemAmount += parseInt(item.amount);
      });

      this.isLoading[0] = false;
    });
  }

  loadData() {
    if(this.EntityType === 1) {
      this.httpService.get2(`/api/citizens/GetCurrent`, this.token).subscribe(r => {
        console.log("erre", r);
        this.citizen = r;
        this.citizenId = r.id;
        this.region = r.region;
        this.country = r.country;
        this.HospitalQ = r.region.hospitalQ;
      });
      this.loadCitizenStats();
    }
  }

  loadCitizenStats() {
    this.httpService.get2(`/api/citizen-stats/GetCurrent`, this.token).subscribe(r => {
      this.citizenStats = r;
    });
  }

  getStars(quality) {
    var s = "";
    for(var j = 1; j <= quality; j++) {
      s += String.fromCodePoint(9733);
    }
    return s;
  }

  useHospital() {
    this.isLoading[2] = true;
    this.httpService.post2(`/api/regions/UseHospital`, {}, this.token).subscribe(r => {
      this.message.success(r.message);
      this.isLoading[2] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[2] = false;
    });
  }

  useJuice() {
    this.isLoading[3] = true;
    this.httpService.post2(`/api/citizens/UseJuice`, {}, this.token).subscribe(r => {
      this.message.success(r.message);
      this.isLoading[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }

  ChangeItemUsagePriority(itemId, quality, amount) {
    this.isLoading[3] = true;
    console.log("docmunet", document.getElementsByName("amount")[0]);
    this.httpService.get2(`/api/citizen-stats/ChangeItemUsagePriority/${this.citizenId}/${itemId}/${quality}/${(document.getElementsByName("amount")[0] as HTMLInputElement).value}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadCitizenStats();
      this.isLoading[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[3] = false;
    });
  }

}
