import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from '../services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-travel',
  templateUrl: './travel.component.html',
  styleUrls: ['./travel.component.css']
})
export class TravelComponent implements OnInit {
  token: string;
  regionId: number;
  hostUrl = environment.hostUrl;
  isLoading = [true, false]

  countries: any[] = [];
  tickets: any[] = [];
  selectedRegion = 0;
  travelInfo = undefined;
  selectedTicketQ = 0;
  entityInfo: any = {};

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.regionId = +this.route.snapshot.paramMap.get("regionId");
    this.loadData();
  }

  loadData() {
    this.selectedRegion = 0;
    this.selectedTicketQ = 0;
    this.isLoading[0] = true;
    this.httpService.get2(`/api/countries/GetAllWithRegions`, this.token).subscribe(r => {
      this.countries = r;
      if (this.regionId > 0)
      {
        this.regionSelected(this.regionId);
        this.selectedRegion = this.regionId;
      }
      this.isLoading[0] = false;
    });
    this.httpService.get2(`/api/inventory/GetByTokenItemId/8`, this.token).subscribe(r => {
      this.tickets = r;
      this.tickets.forEach(ticket => {
        ticket.qualityStars = "";
        for (let i = 1; i <= ticket.quality; i++) {
          ticket.qualityStars = ticket.qualityStars + "&#9733;";
        }
      });
    });
    this.httpService.get2(`/api/citizens/getCurrent`, this.token).subscribe(r => {
      this.entityInfo = r;
    });
  }

  regionSelected(value) {
    this.isLoading[1] = true;
    this.travelInfo = {};
    this.httpService.get2(`/api/travel/getTravelInfo?regionId=${value}`, this.token).subscribe(r => {
      this.travelInfo = r;
      this.travelInfo.ticketQStars = "";
      for (let i = 1; i <= this.travelInfo.ticketQuality; i++) {
        this.travelInfo.ticketQStars = this.travelInfo.ticketQStars + "&#9733;";
      }
      this.travelInfo.hospitalQStars = "";
      for (let i = 1; i <= this.travelInfo.hospitalQuality; i++) {
        this.travelInfo.hospitalQStars = this.travelInfo.hospitalQStars + "&#9733;";
      }
      this.isLoading[1] = false;
    });
  }

  travel() {
    this.isLoading[1] = true;
    this.httpService.post2(`/api/travel/ChangeLocation`, { regionId: this.selectedRegion, ticketChosen: this.selectedTicketQ }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadData();
      this.travelInfo = undefined;
      this.isLoading[1] = false;
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
    });

  }
}
