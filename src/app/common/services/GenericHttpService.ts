import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GenericHttpService {

  constructor(
    private http: HttpClient,
  ) { }

  get(path): Observable<any> {
    return this.http.get<any>(`${environment.apiUrl}${path}`);
  }

  get2(path, token = ''): Observable<any> {
    return this.http.get<any>(`${environment.newApiUrl}${path}`, { headers: { 'token': `${token}` } } );
  }

  post(path, payload): Observable<any> {
    return this.http.post<any>(`${environment.apiUrl}${path}`, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json; charset=utf-8;' } });
  }

  post2(path, payload, token = ''): Observable<any> {
    return this.http.post<any>(`${environment.newApiUrl}${path}`, JSON.stringify(payload), { headers: { 'Content-Type': 'application/json; charset=utf-8;', 'token': token } });
  }

  delete(path, token = ''): Observable<any> {
    return this.http.delete<any>(`${environment.newApiUrl}${path}`, { headers: { 'token': token } });
  }
}
