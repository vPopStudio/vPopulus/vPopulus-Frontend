import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient, HttpResponse } from "@angular/common/http";
import { TranslateLoader } from '@ngx-translate/core';
import { Observable } from 'rxjs';


@Injectable()
export class CustomTranslateLoader implements TranslateLoader {
    contentHeader = new HttpHeaders({ "Content-Type": "application/json", "Access-Control-Allow-Origin": "*" });

    constructor(private http: HttpClient) { }
    getTranslation(lang: string): Observable<any> {
        var apiAddress = "https://i18n.vpopstudio.eu/api/main/export/2/" + lang;
        return Observable.create(observer => {
            // { headers: this.contentHeader }
            this.http.get(apiAddress).subscribe((res: any) => {
                observer.next(res);
                console.log(res)
                observer.complete();
            },
                error => {
                    //  failed to retrieve from api, switch to local
                    this.http.get("/assets/i18n/" + lang + ".json").subscribe((res: any) => {
                        observer.next(res);
                        observer.complete();
                    })
                }
            );
        });
    }
}