import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccomodationViewComponent } from './accomodation-view.component';

describe('AccomodationViewComponent', () => {
  let component: AccomodationViewComponent;
  let fixture: ComponentFixture<AccomodationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccomodationViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccomodationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
