import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { NzModalRef, NzMessageService } from 'ng-zorro-antd';
import { GenericHttpService } from '../../services/GenericHttpService';

@Component({
  selector: 'app-accommodation-management',
  templateUrl: './accommodation-management.component.html',
  styleUrls: ['./accommodation-management.component.css']
})
export class AccommodationManagementComponent implements OnInit {

  @Input() constructionType;
  @Input() constructionId;
  @Input() token;

  constructionInfo = {};
  sellOffer = { };

  isLoading = true;
  price = 0;
  currencyId = 1;
  currencies: any[] = [];

  sellPrice: 0;
  sellCurrencyId: 1;

  constructor(
    private httpService: GenericHttpService,
    private message: NzMessageService,
  ) { }

  ngOnInit() {
    switch (this.constructionType) {
      case 0:
        this.constructionType = 1;
        break;
      case 1:
        this.constructionType = 10;
        break;
    }

    this.httpService.get2(`/api/countries/list`).subscribe(r => {
      this.currencies = r;
      this.currencies.push({ currencyName: "Gold", currencyId: 1 });
      this.currencies = this.currencies.reverse();
      this.loadData();
    });
  }

  loadData() {
    this.isLoading = true;
    if (this.constructionType === 10) {
      this.httpService.get2(`/api/hotel/getManage/${this.constructionId}`, this.token).subscribe(r => {
        this.constructionInfo = r;
        this.isLoading = false;
      });
    }
    this.httpService.get2(`/api/market/estate/GetSingle/${this.constructionType}/${this.constructionId}`, this.token).subscribe(r => {
      this.sellOffer = r;
      this.isLoading = false;
    });
  }

  addRoomOffer() {
    this.isLoading = true;
    this.httpService.post2(`/api/hotel/addRoomOffer`, { hotelId: this.constructionId, price: this.price, currencyId: this.currencyId }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadData();
    });
  }

  removeRoomOffer() {
    this.isLoading = true;
    this.httpService.post2(`/api/hotel/removeRoomOffer`, { hotelId: this.constructionId }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadData();
    });
  }

  // SELLING

  sellConstruction() {
    this.isLoading = true;
    this.httpService.post2(`/api/market/estate/PostOffer`, { constructionType: this.constructionType, constructionId: this.constructionId, price: this.sellPrice, currencyId: this.sellCurrencyId }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadData();
    });
  }

  removeSellOffer() {
    this.isLoading = true;
    this.httpService.post2(`/api/market/estate/remove`, { constructionType: this.constructionType, constructionId: this.constructionId }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadData();
    });
  }

}
