import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from '../services/GenericHttpService';
import { environment } from 'src/environments/environment';
import { NzMessageService, NzModalService } from 'ng-zorro-antd';
import { AccommodationManagementComponent } from './accommodation-management/accommodation-management.component';

@Component({
  selector: 'app-accomodation-view',
  templateUrl: './accomodation-view.component.html',
  styleUrls: ['./accomodation-view.component.css']
})
export class AccomodationViewComponent implements OnInit {

  token = '';
  constructionPacksList = [];
  constructions = [];
  hostUrl = environment.hostUrl;
  isLoading = [true, false, true];

  houseQuality = 0;
  hotelQuality = 0;
  hospitalQuality = 0;
  dsQuality = 0;

  selectedTab = 0;
  isManageVisible = false;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,
    private message: NzMessageService,
    private modalService: NzModalService,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");
    this.loadData();
  }

  loadData(constructionType = null) {
    if(constructionType === null) {
      constructionType = this.selectedTab === 0 ? 1 : (this.selectedTab === 1 ? 10 : 11) 
    }
    this.isLoading[0] = true;
    this.isLoading[2] = true;
    this.httpService.get2(`/api/inventory/GetByTokenItemId/14`, this.token).subscribe(r => {
      this.constructionPacksList = [];
      var stars = "&#9733;";
      for (let i = 1; i <= 5; i++) {
        var found = r.filter(function (el) { return el.quality == i; });
        this.constructionPacksList.push({ quality: stars, amount: found.length > 0 ? found[0].amount : 0 });
        stars = stars + "&#9733;";
      }
      this.isLoading[2] = false;
    });
    this.selectedTab = constructionType == 1 ? 0 : (constructionType == 10 ? 1 : 2);
    this.httpService.get2(`/api/constructions/getlist/${constructionType}`, this.token).subscribe(r => {
      this.constructions = r;
      this.constructions.forEach(construction => {
        var stars = "";
        for (let i = 1; i <= construction.quality; i++) {
          stars = stars + "&#9733;";
        }
        construction.quality = stars;
      });
      this.isLoading[0] = false;
    });
  }

  use(constructionType, id) {
    this.isLoading[0] = true;
    switch (constructionType) {
      case "house":
        this.httpService.post2(`/api/house/usehouse`, { houseId: id }, this.token).subscribe(r => {
          this.message.success(r.message);
          this.loadData();
        }, error => {
          this.message.error(error.error.message);
          this.loadData();
        });
        break;
      case "hotel":
        this.httpService.post2(`/api/hotel/usehotel`, { hotelId: id }, this.token).subscribe(r => {
          this.message.success(r.message);
          this.loadData();
        }, error => {
          this.message.error(error.error.message);
          this.loadData();
        });
        break;
    }
  }

  constructHouse() {
    if (this.houseQuality <= 0) {
      this.message.error("Please select quality!")
      return;
    }

    this.isLoading[1] = true;
    this.httpService.post2(`/api/house/ConstructHouse`, { quality: this.houseQuality }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.houseQuality = 0;
      this.isLoading[1] = false;
      this.loadData(1);
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
    });
  }

  constructHotel() {
    if (this.hotelQuality <= 0) {
      this.message.error("Please select quality!")
      return;
    }

    this.isLoading[1] = true;
    this.httpService.post2(`/api/hotel/ConstructHotel`, { quality: this.hotelQuality }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.hotelQuality = 0;
      this.isLoading[1] = false;
      this.loadData(10);
    }, error => {
      this.message.error(error.error.message);
      this.isLoading[1] = false;
    });
  }

  showManage(id) {
    this.modalService.create({
      nzTitle: 'Manage construction',
      nzContent: AccommodationManagementComponent,
      nzComponentParams: {
        constructionType: this.selectedTab,
        constructionId: id,
        token: this.token
      },
      nzOkDisabled: true
    });
  }

}
