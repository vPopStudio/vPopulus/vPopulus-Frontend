import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';
import { HubConnection, HubConnectionBuilder, HttpTransportType } from '@aspnet/signalr';
import { NzMessageService } from 'ng-zorro-antd';
import * as introJs from 'intro.js/intro.js';
import { TranslateService } from '@ngx-translate/core';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-battle',
  templateUrl: './battle.component.html',
  styleUrls: ['./battle.component.css']
})
export class BattleComponent implements OnInit {

  hostUrl: any;
  battle: any = {};
  battleAttackerAllies = [];
  battleDefenderAllies = [];
  battleUser: any;

  battleId: string;
  token: string;
  timeLeft: string = '';
  wallPercentages: number[] = [50, 50];
  battleHeros: any[] = [{}, {}];
  wallSize: string = '';
  attackers: any[];
  defenders: any[];
  loadings: boolean[] = [false, false, false, false, false, false]; // Buying wellness pack |
  spectators: any[] = [];

  prioritySettings = {
    juices: {
      available: [],
      selectedQ: 1,
    },
    weapons: {
      available: [],
      selectedQ: 2,
    }
  };

  introJS = introJs();

  _battleHubConnection: HubConnection;

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
    private message: NzMessageService,
    private translate: TranslateService,
    private cookies: CookieService,
  ) { }

  ngOnInit() {
    this.hostUrl = environment.hostUrl;
    this.battleId = this.route.snapshot.paramMap.get("battleId");
    this.token = this.route.snapshot.paramMap.get("token");

    this.loadSignalR();
    this.loadData();
  }

  loadTutorial() {
    if (this.cookies.get("battleTutorialDone") === "true")
      return;
    var promises = [];
    promises.push(new Promise((resolve) => { this.translate.get("Social.Tutorial.Battle1").subscribe(r => { return resolve(r) }) }));
    promises.push(new Promise((resolve) => { this.translate.get("Social.Tutorial.Battle3").subscribe(r => { return resolve(r) }) }));
    promises.push(new Promise((resolve) => { this.translate.get("Social.Tutorial.Battle4").subscribe(r => { return resolve(r) }) }));
    promises.push(new Promise((resolve) => { this.translate.get("Social.Tutorial.Battle2").subscribe(r => { return resolve(r) }) }));

    Promise.all(promises).then(r => {
      this.introJS.setOptions({
        exitOnOverlayClick: false,
        steps: [
          { element: '#step1', intro: r[0] },
          { element: '#step2', intro: r[1] },
          { element: '#step3', intro: r[2] },
          { element: '#battlePageTop', intro: r[3] },
        ]
      });

      this.introJS.start();
      this.introJS.oncomplete(() => {
        this.cookies.set("battleTutorialDone", "true");
      });
      this.introJS.onexit(() => {
        this.cookies.set("battleTutorialDone", "true");
      });
    });
  }

  loadData() {
    this.loadings[4] = true;
    this.loadings[5] = true;
    this.httpService.get2(`/api/military/battles/${this.battleId}`).subscribe(battle => {
      this.battle = battle;
      this.httpService.get2(`/api/military/battles/allies/${this.battleId}`, this.token).subscribe(allies => {
        this.battleAttackerAllies = allies.filter(x => x.countryId == this.battle.battle.attackerId);
        this.battleDefenderAllies = allies.filter(x => x.countryId == this.battle.battle.defenderId);
      });

      this.loadings[4] = false;
      this.loadUserData();
    });
  }

  loadUserData() {
    if (this.battle.battle.winnerId === null) {
      this.loadings[5] = true;
      this.httpService.get2(`/api/military/battles/GetForCitizen/${this.battleId}`, this.token).subscribe(battleUser => {
        this.battleUser = battleUser;
        this.httpService.get2(`/api/inventory`, this.token).subscribe(items => {
          this.prioritySettings.juices.available = items.filter(x => x.itemId == 4).reverse();
          this.prioritySettings.juices.available.push({ quality: 0 });
          this.prioritySettings.juices.available = this.prioritySettings.juices.available.reverse();

          this.prioritySettings.weapons.available = items.filter(x => x.itemId == 6);
          this.prioritySettings.weapons.available.push({ quality: 0 });
          this.prioritySettings.weapons.available = this.prioritySettings.weapons.available.reverse();
          console.log(this.prioritySettings)
          this.httpService.get2(`/api/citizen-stats/getCurrent`, this.token).subscribe(citizenStats => {
            this.prioritySettings.weapons.selectedQ = citizenStats.weaponPriority;
            this.prioritySettings.juices.selectedQ = citizenStats.juicePriority;
          });
          this.loadings[5] = false;
        });
        this.loadTutorial();
      });
    }
  }

  loadSignalR() {
    this._battleHubConnection = new HubConnectionBuilder().withUrl(environment.newApiUrl + '/militaryHub')
      // .configureLogging(LogLevel.Debug)
      .build();
    this._battleHubConnection.start().catch((e) => { return console.error(e) }).then(() => {
      this._battleHubConnection.send("PlayerConnected", this.battleId, this.token);
    });
    this._battleHubConnection.on("updateTimer", (text) => {
      this.timeLeft = text;
    });
    this._battleHubConnection.on("updateWall", (text) => {
      var values = text.split('|');
      this.wallPercentages = [Number(values[0]), Number(values[1])];
      this.wallSize = values[2];
    });
    this._battleHubConnection.on("updateFightersAttackers", (text) => {
      this.attackers = JSON.parse(text);
    });
    this._battleHubConnection.on("updateFightersDefenders", (text) => {
      this.defenders = JSON.parse(text);
    });
    this._battleHubConnection.on("updateHeroAttacker", (text) => {
      this.battleHeros[0] = JSON.parse(text);
    });
    this._battleHubConnection.on("updateHeroDefender", (text) => {
      this.battleHeros[1] = JSON.parse(text);
    });
    this._battleHubConnection.on("updateSpectators", (text) => {
      this.spectators = JSON.parse(text);
    });
  }

  useWellnessPack() {
    this.loadings[0] = true;
    this.httpService.get2(`/api/store/UseWellnessPack`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
      this.loadings[0] = false;
    }, error => {
      this.message.error(error.error.message);
      this.loadings[0] = false;
    });
  }

  useHospital() {
    this.loadings[2] = true;
    this.httpService.post2(`/api/regions/UseHospital`, {}, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
      this.loadings[2] = false;
    }, error => {
      this.message.error(error.error.message);
      this.loadings[2] = false;
    });
  }

  useJuice() {
    this.loadings[3] = true;
    this.httpService.post2(`/api/citizens/UseJuice`, {}, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
      this.loadings[3] = false;
    }, error => {
      this.message.error(error.error.message);
      this.loadings[3] = false;
    });
  }

  notOk() { }

  fight(sideId = 0) {
    this.loadings[1] = true;
    this.httpService.post2(`/api/military/battles/fight`, { battleId: this.battleId, sideId: sideId }, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
      this.loadings[1] = false;
    }, error => {
      this.message.error(error.error.message);
      this.loadings[1] = false;
    });
  }

  setWeaponPriority(quality) {
    this.loadings[5] = true;
    this.httpService.get2(`/api/citizen-stats/ChangeItemUsagePriority/6/${quality}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
    });
  }

  setJuicePriority(quality) {
    this.loadings[5] = true;
    this.httpService.get2(`/api/citizen-stats/ChangeItemUsagePriority/4/${quality}`, this.token).subscribe(r => {
      this.message.success(r.message);
      this.loadUserData();
    });
  }

}
