import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-wars-list',
  templateUrl: './wars-list.component.html',
  styleUrls: ['./wars-list.component.css']
})
export class WarsListComponent implements OnInit {
  hostUrl = environment.hostUrl;
  token;
  isLoading = false;

  wars: any[] = [];

  constructor(
    private httpService: GenericHttpService,
    private route: ActivatedRoute,
  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");

    this.isLoading = true;
    this.httpService.get(`/api/military/wars`).subscribe(r => {
      this.wars = r.list;
      this.isLoading = false;
    });
  }

  
}
