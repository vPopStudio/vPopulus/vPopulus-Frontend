import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  token;
  hostUrl = environment.hostUrl;

  isVotingDay = false;
  upcomingElectionsType = 0;

  armyOrders;

  constructor(
    private route: ActivatedRoute,
    private httpService: GenericHttpService,

  ) { }

  ngOnInit() {
    this.token = this.route.snapshot.paramMap.get("token");

    this.httpService.get2(`/api/elections/isvotedaytoday`, this.token).subscribe(r => {
      this.isVotingDay = r;
    });

    this.httpService.get2(`/api/elections/upcoming`, this.token).subscribe(r => {
      this.upcomingElectionsType = r;
    });

    // this.httpService.get2(`/api/armies/orders/global`, this.token).subscribe(r => {
    //   this.armyOrders = r;
    //   console.log(this.armyOrders);
    // });
  }

}
