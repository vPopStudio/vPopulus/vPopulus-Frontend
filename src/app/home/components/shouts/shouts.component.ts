import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { NzMessageService } from 'ng-zorro-antd';

@Component({
  selector: 'app-home-shouts',
  templateUrl: './shouts.component.html',
  styleUrls: ['./shouts.component.css']
})
export class ShoutsComponent implements OnInit {
  @Input() token;
  hostUrl = environment.hostUrl;
  isLoading = false;
  currentTab = 1;

  entity;

  newShoutMessage;
  shoutReplyMessage;
  channel;
  channelId;

  constructor(
    private httpService: GenericHttpService,
    private messageService: NzMessageService,
  ) { }

  ngOnInit() {
    this.httpService.get2(`/api/auth/GetData/${this.token}`).subscribe(r => {
      this.entity = r;
      this.loadData();
    });
  }

  loadData() {
    if (this.channelId == undefined) {
      this.channelId = 6;
      if (this.currentTab == 0) {
        this.channelId = this.entity.entityType == 1 ? 7 : 4;
      } else if (this.currentTab == 2) {
        this.channelId = 5;
      }
    }

    this.isLoading = true;
    this.httpService.get2(`/api/shouts/homepage/${this.channelId}?page=1`, this.token).subscribe(r => {
      this.channel = r;
      this.channel.shouts.forEach(shout => {
        shout.isExpanded = false;
      });

      this.isLoading = false;
    });
  }

  changeTab() {
    this.channelId = 6;
    if (this.currentTab == 0) {
      this.channelId = this.entity.entityType == 1 ? 7 : 4;
    } else if (this.currentTab == 2) {
      this.channelId = 5;
    }

    this.loadData();
  }

  postShout() {
    this.isLoading = true;
    this.httpService.post2(`/api/shouts`, { channelId: this.channelId, message: this.newShoutMessage, recieverType: this.channel.target }, this.token).subscribe(r => {
      if (r.isSuccessful == true) {
        this.messageService.success(r.message);
        this.shoutReplyMessage = '';
      } else {
        this.messageService.error(r.message);
      }
      this.newShoutMessage = '';
      this.loadData();
    });
  }

  replyShout(shoutId) {
    this.isLoading = true;
    this.httpService.post2(`/api/shouts/reply`, { shoutId: shoutId, message: this.shoutReplyMessage }, this.token).subscribe(r => {
      if (r.isSuccessful == true) {
        this.messageService.success(r.message);
        this.shoutReplyMessage = '';
      } else {
        this.messageService.error(r.message);
      }
      this.loadData();
    });
  }

  likeShout(shoutId) {
    this.isLoading = true;
    this.httpService.post2(`/api/shouts/like`, { shoutId: shoutId }, this.token).subscribe(r => {
      if (r.isSuccessful == true) {
        this.messageService.success(r.message);
      } else {
        this.messageService.error(r.message);
      }
      this.loadData();
    });
  }
}
