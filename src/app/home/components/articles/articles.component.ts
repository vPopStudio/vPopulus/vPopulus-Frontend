import { Component, OnInit, Input } from '@angular/core';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-home-articles',
  templateUrl: './articles.component.html',
  styleUrls: ['./articles.component.css']
})
export class ArticlesComponent implements OnInit {
  @Input() token;
  hostUrl = environment.hostUrl;
  isLoading = false;
  currentTab = 1;
  adminInsider;

  list = [];
  constructor(
    private httpService: GenericHttpService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/articles/homepage/${this.currentTab}`, this.token).subscribe(r => {
      this.list = r;
    });

    this.httpService.get2(`/api/articles/lastInsider`, this.token).subscribe(r => {
      this.adminInsider = r;
      this.isLoading = false;
    });

  }

  changeTab() {
    this.loadData();
  }
}
