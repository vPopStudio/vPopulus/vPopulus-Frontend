import { Component, OnInit, Input } from '@angular/core';
import { environment } from 'src/environments/environment';
import { GenericHttpService } from 'src/app/common/services/GenericHttpService';

@Component({
  selector: 'app-home-battles',
  templateUrl: './battles.component.html',
  styleUrls: ['./battles.component.css']
})
export class BattlesComponent implements OnInit {
  @Input() token;
  hostUrl = environment.hostUrl;
  isLoading = false;
  currentTab = 1;

  list = [];
  constructor(
    private httpService: GenericHttpService
  ) { }

  ngOnInit() {
    this.loadData();
  }

  loadData() {
    this.isLoading = true;
    this.httpService.get2(`/api/military/battles/homepage/${this.currentTab}`, this.token).subscribe(r => {
      this.list = r;
      this.isLoading = false;
    });
  }

  changeTab() {
    this.loadData();
  }

}
