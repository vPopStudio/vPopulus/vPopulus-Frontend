import { Component, OnInit } from '@angular/core';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css']
})
export class LandingPageComponent implements OnInit {

  hostUrl = environment.hostUrl;

  topCountries: any[] = [];
  topCitizens: any[] = [];
  languages: any[] = [];

  constructor(
    // private homeService: HomeService,
  ) { }

  ngOnInit() {
    // this.homeService.getLandingPage().subscribe(r => {
    //   this.topCountries = r.countries;
    //   this.topCitizens = r.citizensTop5;
    //   this.languages = r.languages;
    // });
  }

}
