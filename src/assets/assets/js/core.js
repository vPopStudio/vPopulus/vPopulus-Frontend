String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

function BBCode(type, elementID) {
    var textComponent = document.getElementById(elementID);
    var selectedText = null;
    if (document.selection != undefined) { // Internet explorer
        textComponent.focus();
        var sel = document.selection.createRange();
        selectedText = sel.text;
    }
    else if (textComponent.selectionStart != undefined) { // Mozilla and chrome and opera.
        var startPos = textComponent.selectionStart;
        var endPos = textComponent.selectionEnd;
        selectedText = textComponent.value.substring(startPos, endPos)
    }

    if (selectedText.length >= 1) {
        var n = null;
        switch (type) {
            case "b":
                n = textComponent.value.splice(startPos, selectedText.length, "[b]" + selectedText + "[/b]");
                break;
            case "i":
                n = textComponent.value.splice(startPos, selectedText.length, "[i]" + selectedText + "[/i]");
                break;
            case "u":
                n = textComponent.value.splice(startPos, selectedText.length, "[u]" + selectedText + "[/u]");
                break;
            case "p":
                n = textComponent.value.splice(startPos, selectedText.length, "[p]" + selectedText + "[/p]");
                break;
            case "s":
                n = textComponent.value.splice(startPos, selectedText.length, "[del]" + selectedText + "[/del]");
                break;
            case "hidden":
                n = textComponent.value.splice(startPos, selectedText.length, "[hidden]" + selectedText + "[/hidden]");
                break;
            case "url":
                var url = prompt("Please enter the url", "Url");
                n = textComponent.value.splice(startPos, selectedText.length, "[url]" + url + "|" + selectedText + "[/url]");
                break;
            case "img":
                var url = prompt("Please enter the url of image.", "Image url");
                n = textComponent.value.splice(startPos, selectedText.length, "[img]" + url + "[/img]");
                break;
            case "aLeft":
                n = textComponent.value.splice(startPos, selectedText.length, "[aleft]" + selectedText + "[/aleft]");
                break;
            case "aCenter":
                n = textComponent.value.splice(startPos, selectedText.length, "[acenter]" + selectedText + "[/acenter]");
                break;
            case "aRight":
                n = textComponent.value.splice(startPos, selectedText.length, "[aright]" + selectedText + "[/aright]");
                break;
            case "youtube":
                var url = prompt("Youtube url|Width|Height", "Youtube embed");
                n = textComponent.value.splice(startPos, selectedText.length, "[youtube]" + url + "[/youtube]");
                break;
        }

        document.getElementById(elementID).value = n;
        document.getElementById(elementID).innerHTML = n;
    }
}


//* SCROLL FUNCTION *//
//$(function() {
//    $('#scroll').slimScroll({
//        height: 'calc(100% - 40px)',
//        height: '-moz-calc(100% - 40px)', /* Firefox */
//                height: '-webkit-calc(100% - 40px)' /* Chrome, Safari */
//    });
//});

//* SCROLL FUNCTION VIA BUTTONS (FOR THOSE WITH NO MOUSESCROLL) *//
var scrolled = 0;

$(document).ready(function() {

    $("#scrollDown").on("click", function() {
        scrolled = scrolled + 150;

        $(".sidebarR_fixed").animate({
            scrollTop: scrolled
        });
    });

    $("#scrollUp").on("click", function() {
        scrolled = scrolled - 150;

        $(".sidebarR_fixed").animate({
            scrollTop: scrolled
        });
    });

});

//* SHOW/HIDE FUNCTION FOR CONTENTBOX'S *//
function showhide(id) {
    var e = document.getElementById(id);
    if (e.style.display == 'block')
        e.style.display = 'none';
    else
        e.style.display = 'block';
}

jQuery(document).ready(function () {
    jQuery(".events-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.events-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".battles-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.battles-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".orders-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.orders-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".shouts-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.shouts-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".playerNews-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.playerNews-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".citizenMedals-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.citizenMedals-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".accomodationHouses-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.accomodationHouses-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
    jQuery(".exchange-tabs .tab-links a").on('click', function (e) {
        var currentAttrValue = jQuery(this).attr('href');

        // Show/Hide Tabs
        jQuery('.exchange-tabs ' + currentAttrValue).show().siblings().hide();

        // Change/remove current tab to active
        jQuery(this).parent('li').addClass('active').siblings().removeClass('active');

        e.preventDefault();
    });
});

$(document).ready(function () {

    var id = '#dialog';

    //Get the screen height and width
    var maskHeight = $(document).height();
    var maskWidth = $(window).width();

    //Set heigth and width to mask to fill up the whole screen
    $('#mask').css({ 'width': maskWidth, 'height': maskHeight });

    //transition effect		
    $('#mask').fadeIn(500);
    $('#mask').fadeTo("slow", 0.9);

    //Get the window height and width
    var winH = $(window).height();
    var winW = $(window).width();

    //Set the popup window to center
    $(id).css('top', winH / 2 - $(id).height() / 2);
    $(id).css('left', winW / 2 - $(id).width() / 2);

    //transition effect
    $(id).fadeIn();

    //if close button is clicked
    $('.window .close').click(function (e) {
        //Cancel the link behavior
        e.preventDefault();

        $('#mask').hide();
        $('.window').hide();
    });

    //if mask is clicked
    $('#mask').click(function () {
        $(this).hide();
        $('.window').hide();
    });

});