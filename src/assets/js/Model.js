﻿var ModalOpening = false;

function openModal(_url, _data) {
    if (ModalOpening === true)
        return;
    ModalOpening = true;
    $.ajax({
        url: _url,
        data: _data,
        success: function (result) {
            $("#modalPlacement").html(result);
            //$('#myModal').modal('show');
            ModalOpening = false;
        },
        error: function () {
            ModalOpening = false;
            window.reload();
        }
    });
}

function loadPage(_url, _data, _div) {
    $.ajax({
        url: _url,
        data: _data,
        success: function (result) {
            $(_div).html(result);
        },
        error: function () {
        }
    });
}

function closeModal() {
    $('.modal').modal('hide');
    $('.modal-backdrop').remove();
}