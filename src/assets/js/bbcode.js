﻿String.prototype.splice = function (idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
};

function BBCode(type, elementID) {
    var textComponent = document.getElementById(elementID);
    var selectedText = null;
    if (document.selection != undefined) { // Internet explorer
        textComponent.focus();
        var sel = document.selection.createRange();
        selectedText = sel.text;
    }
    else if (textComponent.selectionStart != undefined) { // Mozilla and chrome and opera.
        var startPos = textComponent.selectionStart;
        var endPos = textComponent.selectionEnd;
        selectedText = textComponent.value.substring(startPos, endPos)
    }

    if (selectedText.length >= 1) {
        var n = null;
        switch(type)
        {
            case "b":
                n = textComponent.value.splice(startPos, selectedText.length, "[b]" + selectedText + "[/b]");
                break;
            case "i":
                n = textComponent.value.splice(startPos, selectedText.length, "[i]" + selectedText + "[/i]");
                break;
            case "u":
                n = textComponent.value.splice(startPos, selectedText.length, "[u]" + selectedText + "[/u]");
                break;
            case "p":
                n = textComponent.value.splice(startPos, selectedText.length, "[p]" + selectedText + "[/p]");
                break;
            case "s":
                n = textComponent.value.splice(startPos, selectedText.length, "[del]" + selectedText + "[/del]");
                break;
            case "hidden":
                n = textComponent.value.splice(startPos, selectedText.length, "[hidden]" + selectedText + "[/hidden]");
                break;
            case "url":
                var url = prompt("Please enter the url", "Url");
                n = textComponent.value.splice(startPos, selectedText.length, "[url]" + url + "|" + selectedText + "[/url]");
                break;
            case "img":
                var url = prompt("Please enter the url of image.", "Image url");
                n = textComponent.value.splice(startPos, selectedText.length, "[img]" + url + "[/img]");
                break;
            case "aLeft":
                n = textComponent.value.splice(startPos, selectedText.length, "[aleft]" + selectedText + "[/aleft]");
                break;
            case "aCenter":
                n = textComponent.value.splice(startPos, selectedText.length, "[acenter]" + selectedText + "[/acenter]");
                break;
            case "aRight":
                n = textComponent.value.splice(startPos, selectedText.length, "[aright]" + selectedText + "[/aright]");
                break;
            case "youtube":
                var url = prompt("Youtube url|Width|Height", "Youtube embed");
                n = textComponent.value.splice(startPos, selectedText.length, "[youtube]" + url + "[/youtube]");
                break;
        }

        document.getElementById(elementID).value = n;
        document.getElementById(elementID).innerHTML = n;
    }
}
