export const environment = {
  apiUrl: 'http://localhost:44364',
  newApiUrl: 'http://localhost:44364',
  hostUrl: 'http://localhost:44364',
  production: true,
  experimental: false
};
