export const environment = {
  apiUrl: 'https://dev.vpopulus.net',
  newApiUrl: 'https://api-dev.vpopulus.net',
  hostUrl: 'https://dev.vpopulus.net',
  production: true, 
  showSideAndTopBar: false,
  experimental: false, 
};
